#include <iostream>
#include <cassert>
#include <cstring>
#include <thread>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "networking.h"
#include "group.h"
#include "elgamal.h"
#include "comp_model.h"
#include "cgate.h"
#include "add.h"
#include "arith.h"
#include "condorcet.h"

GlobalContext Context;
std::mutex ctx_mtx;
unsigned long COUNT_POW = 0;

void send_worker(NodeDAG * nodeDag) {
    Worker wk(SEND, *nodeDag);
    wk.run();
}

void recv_worker(NodeDAG * nodeDag) {
    Worker wk(RECV, *nodeDag);
    wk.run();
}

void comp_worker(NodeDAG * nodeDag) {
    Worker wk(COMP, *nodeDag);
    wk.run();
}

std::vector<DataKey> DeclareName(std::string prefix, int k) {
    std::vector<DataKey> out;
    for (int i = 0; i < k; ++i) {
        DataKey name = prefix + std::to_string(i);
        out.push_back(name);
    }
    return out;
}

std::vector<std::vector<std::vector<DataKey>>> ReadBallots(
        std::string pref, int k) {
    int m = 1;
    while ( (k-1) > (1<<m)-1 )
        m++;

    std::vector<std::vector<std::vector<DataKey>>> Ballots;
    int c = 0;
    while (1) {
        std::vector<std::vector<DataKey>> Bal;
        std::string line;
        std::string name = pref + std::to_string(c) + ",";

        bool end = false;
        for (int i = 0; i < k; ++i) {
            Bal.push_back(std::vector<DataKey> { });
            for (int j = 0; j < m; ++j) {
                std::string line;
                if (!std::getline(std::cin, line)) {
                    end = true;
                    break;
                }
                Ciphertext c(line.c_str());
                std::string key = name + std::to_string(i) + ","
                    + std::to_string(j);
                Bal[i].push_back(key);
                Context.data[key] = c;
                // Skip the ZKP. Assumes that they have been checked
                // offline.
                std::getline(std::cin, line);
            }
            if (end)
                break;
        }
        if (end)
            break;
        Ballots.push_back(Bal);
        c++;
    }
    std::cerr << "Read " << c << " ballots\n";
    return Ballots;
}


// Usage:
//   ./cond_trustee 0 sk 4 < ballots_file
// where:
//   0  is the number of the trustee (same order as in the conf file)
//   sk is the public key of the trustee
//   4  is the number of candidates
//   ballots_file contains the ballots
int main(int argc, char *argv[]) {
    assert (argc == 4);
    net_info NI;
    NI.init("conf", atoi(argv[1]));
    Node::init(&NI);

    Zq_elt sk(argv[2]);
    G_elt mypk = G_elt::base().pow(sk);
    // Put my secret key and the general public key in global context
    {
        G_elt pk;
        for (int i = 0; i < NI.pub_keys.size(); ++i) {
            G_elt zz(NI.pub_keys[i].c_str());
            Context.data["pk" + std::to_string(i)] = zz;
            if (i == NI.me)
                assert(zz == mypk);
            pk = pk*zz;
        }
        Context.data["pk"] = pk;
        Context.data["sk"] = sk;
        Context.data["mypk"] = mypk;
        Context.data["me"] = NI.me;
        std::cerr << "Main pk is " << pk << "\n";
    }

    int k = atoi(argv[3]);

    // Read ballots
    std::vector<std::vector<std::vector<DataKey>>> Ballots = 
        ReadBallots("Bal", k);
    
    // Winner
    std::vector<DataKey> Winner = DeclareName("Winner", k);

    //////////////////////////////////////
    // Declare the DAG of MPC computation.
    NodeDAG nodeDag;
    std::vector<Node *> Cond = CondorcetSchulze(&NI, "Condorcet",
            Ballots,
            Winner,
            std::vector<NodeTag> { });
    nodeDag.init();

    ///////////////////////
    // Start worker threads
    for (int i = 0; i < NI.pub_keys.size(); ++i) {
        if (i == NI.me)
            continue;
        std::thread th_recvloop(Node::recv_loop, &NI, i, &nodeDag);
        th_recvloop.detach();
    }
    std::thread th_send(send_worker, &nodeDag);
    std::thread th_recv(recv_worker, &nodeDag);
    std::thread th_comp(comp_worker, &nodeDag);
    std::thread th_comp2(comp_worker, &nodeDag);
    std::thread th_comp3(comp_worker, &nodeDag);
    std::thread th_comp4(comp_worker, &nodeDag);

    // Wait for the MPC computation to finish
    th_send.join();
    th_recv.join();
    th_comp.join();
    th_comp2.join();
    th_comp3.join();
    th_comp4.join();

    ///////////////
    // Print result
    std::cerr << "Condorcet-Schulze winners are: ";
    for (int i = 0; i < Winner.size(); ++i) {
        std::cerr << std::get<G_elt>(Context.data[Winner[i]]).dlog() << " ";
    }
    std::cerr << "\n";

    std::cerr << "Performed " << COUNT_POW << " scalar mult\n";
    std::cerr << "Transcript size is " << TRANSCRIPT_SIZE << " Zq/G elements\n";
    return EXIT_SUCCESS;
}
