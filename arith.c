#include <cassert>

#include "comp_model.h"
#include "elgamal.h"
#include "cgate.h"
#include "add.h"
#include "arith.h"


std::vector<Node *> Aggreg(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int N = X.size();
    // n is the smallest such that N fits in n bits.
    int n = 0;
    while (N > (1<<n)-1)
        n++;
    // Z must be at least this length
    assert (Z.size() >= n);

    // Small cases
    if (X.size() == 2) {
        std::vector<Node *> NN = AddBits(NI, tag + "add",
                std::vector<DataKey> { X[0] },
                std::vector<DataKey> { X[1] },
                Z, dep);
        result.insert(std::end(result), std::begin(NN), std::end(NN));
        return result;
    }
    if (X.size() == 1) {
        std::vector<Node *> NN = AddBits(NI, tag + "add",
                std::vector<DataKey> { X[0] },
                std::vector<DataKey> { },
                Z, dep);
        result.insert(std::end(result), std::begin(NN), std::end(NN));
        return result;
    }

    std::vector<DataKey> Z1;
    std::vector<DataKey> Z2;
    for (int i = 0; i < Z.size()-1; ++i) {
        Z1.push_back(tag + "left"  + std::to_string(i));
        Z2.push_back(tag + "right" + std::to_string(i));
    }
    
    int N2 = N/2;
    int N2b = N-N2;
    std::vector<DataKey> X1;
    std::vector<DataKey> X2;
    for (int i = 0; i < N2; ++i) {
        X1.push_back(X[i]);
    }
    for (int i = N2; i < N; ++i) {
        X2.push_back(X[i]);
    }

    std::vector<NodeTag> newdep;
    std::string tagN2 = tag + std::to_string(N);
    std::vector<Node *> Left = Aggreg(NI, tagN2 + "l", X1, Z1, dep);
    newdep.push_back(Left.back()->ntag);
    result.insert(std::end(result), std::begin(Left), std::end(Left));
    std::string tagN2b = tag + std::to_string(N2b);
    std::vector<Node *> Right = Aggreg(NI, tagN2b + "r", X2, Z2, dep);
    newdep.push_back(Right.back()->ntag);
    result.insert(std::end(result), std::begin(Right), std::end(Right));

    std::vector<Node *> Middle = AddBits(NI, tag + "add", Z1, Z2, Z, newdep);
    result.insert(std::end(result), std::begin(Middle), std::end(Middle));

    return result;
}

std::vector<Node *> AggregBits(net_info *NI, std::string tag,
        std::vector<std::vector<DataKey>> X,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int N = X.size();

    // Small cases
    if (X.size() == 2) {
        std::vector<Node *> NN = AddBits(NI, tag + "add",
                X[0], X[1], Z, dep);
        result.insert(std::end(result), std::begin(NN), std::end(NN));
        return result;
    }
    if (X.size() == 1) {
        std::vector<Node *> NN = AddBits(NI, tag + "add",
                X[0], std::vector<DataKey> { },
                Z, dep);
        result.insert(std::end(result), std::begin(NN), std::end(NN));
        return result;
    }

    std::vector<DataKey> Z1;
    std::vector<DataKey> Z2;
    for (int i = 0; i < Z.size()-1; ++i) {
        Z1.push_back(tag + "left"  + std::to_string(i));
        Z2.push_back(tag + "right" + std::to_string(i));
    }
    
    int N2 = N/2;
    int N2b = N-N2;
    std::vector<std::vector<DataKey>> X1;
    std::vector<std::vector<DataKey>> X2;
    for (int i = 0; i < N2; ++i) {
        X1.push_back(X[i]);
    }
    for (int i = N2; i < N; ++i) {
        X2.push_back(X[i]);
    }

    std::vector<NodeTag> newdep;
    std::string tagN2 = tag + std::to_string(N);
    std::vector<Node *> Left = AggregBits(NI, tagN2 + "l", X1, Z1, dep);
    newdep.push_back(Left.back()->ntag);
    result.insert(std::end(result), std::begin(Left), std::end(Left));
    std::string tagN2b = tag + std::to_string(N2b);
    std::vector<Node *> Right = AggregBits(NI, tagN2b + "r", X2, Z2, dep);
    newdep.push_back(Right.back()->ntag);
    result.insert(std::end(result), std::begin(Right), std::end(Right));

    std::vector<Node *> Middle = AddBits(NI, tag + "add", Z1, Z2, Z, newdep);
    result.insert(std::end(result), std::begin(Middle), std::end(Middle));

    return result;
}


void MulBits_init(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 0);
    assert (out.size() == 1);
    Context.set(out[0], Ciphertext::E0());
}


void MulBits_comp(std::vector<DataKey> out, std::vector<DataKey> inp) {
    int mY = inp.size() - 1;
    assert (out.size() == mY + 2);
    // Set Z[i]
    Context.set(out[mY+1], std::get<Ciphertext>(Context.get(inp[0])));
    // Copy U to T
    for (int j = 0; j < mY; ++j) {
        Context.set(out[j],
                std::get<Ciphertext>(Context.get(inp[j+1])));
    }
}

void MulBits_final(std::vector<DataKey> out, std::vector<DataKey> inp) {
    int mY = inp.size()-1;
    int mX = out.size()-mY;

    for (int i = mX; i < mX+mY; ++i) {
        Context.set(out[i],
                std::get<Ciphertext>(Context.get(inp[i-mX])));
    }
}

std::vector<Node *> MulBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int mX = X.size();
    int mY = Y.size();
    assert (Z.size() == mX + mY);

    std::vector<std::vector<DataKey>> AA;
    for (int i = 0; i < mX; ++i) {
        std::vector<DataKey> Ai;
        std::string tagi = tag + "A" + std::to_string(i) + ",";
        for (int j = 0; j < mY; ++j) {
            Ai.push_back(tagi + std::to_string(j));
        }
        AA.push_back(Ai);
    }

    std::vector<DataKey> T;
    for (int i = 0; i <= mY; ++i) {
        T.push_back(tag + "T" + std::to_string(i));
    }
    std::vector<DataKey> U;
    for (int i = 0; i <= mY; ++i) {
        U.push_back(tag + "U" + std::to_string(i));
    }

    //////////////////////
    // Compute all the Aij = CSZ(Xi, Yj)

    std::vector<NodeTag> curr_dep;
    // Case i = 0
    {
        std::string tagi = tag + "CG0,";
        std::vector<Node *> NN = CGate(NI, tagi + std::to_string(0),
                X[0], Y[0], Z[0], dep);
        for (Node * z : NN)
            result.push_back(z);
        curr_dep.push_back(result.back()->ntag);

        for (int j = 1; j < mY; ++j) {
            NN = CGate(NI, tagi + std::to_string(j),
                    X[0], Y[j], T[j-1], dep);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.push_back(result.back()->ntag);
        }
        // Also set T[mY] = E0
        Node * N = Node::CompNode(NI, tag + "init",
            dep, MulBits_init,
            std::vector<DataKey> { },
            std::vector<DataKey> { T[mY-1] });
        result.push_back(N);
        curr_dep.push_back(N->ntag);
    }
    // Case i = 1.. mX-1
    for (int i = 1; i < mX; ++i) {
        std::string tagi = tag + "CG" + std::to_string(i) + ",";
        for (int j = 0; j < mY; ++j) {
            std::vector<Node *> NN = CGate(NI, tagi + std::to_string(j),
                    X[i], Y[j], AA[i][j], dep);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.push_back(result.back()->ntag);
        }
    }

    // Main loop with Add and Carry propagation
    for (int i = 1; i < mX; ++i) {
        std::vector<DataKey> In1, In2, Out;
        In1.insert(In1.begin(), T.begin(), T.begin() + mY);
        assert (In1.size() == mY);
        std::vector<Node *> NN = AddBits(NI, tag + "Add" + std::to_string(i),
                In1, AA[i], U, curr_dep);
        for (Node * z : NN)
            result.push_back(z);
        std::vector<NodeTag> innerdep = { result.back()->ntag };
        Out.clear();
        Out.insert(Out.begin(), T.begin(), T.end());
        Out.push_back(Z[i]);
        Node * NNN = Node::CompNode(NI, tag + "comp" + std::to_string(i),
            innerdep,
            MulBits_comp,
            U, Out);
        result.push_back(NNN);
        curr_dep.clear();
        curr_dep.push_back(result.back()->ntag);
    }

    // Final copy
    Node * NN = Node::CompNode(NI, tag + "final",
            curr_dep,
            MulBits_final,
            T, Z);
    result.push_back(NN);

    return result;
}

void DivBits_final(std::vector<DataKey> out, std::vector<DataKey> inp) {
    int r = inp.size();
    assert (out.size() == r);
    for (int i = 0 ; i < r; ++i) {
        Context.set(out[i],
                Ciphertext::E1()/std::get<Ciphertext>(Context.get(inp[i])));
    }
}


// Warning: this assume X < Y.
std::vector<Node *> DivBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int m = X.size();
    assert (Y.size() == m);
    int r = Z.size();

    std::vector<DataKey> A, B, R;
    for (int i = 0; i < m; ++i) {
        A.push_back(tag + "A" + std::to_string(i));
        B.push_back(tag + "B" + std::to_string(i));
        R.push_back(tag + "R" + std::to_string(i));
    }

    std::vector<NodeTag> curr_dep = { dep };
    for (int i = 0; i < r; ++i) {
        std::vector<DataKey> _A;
        if (i == 0) {
            _A = X;
        } else {
            _A = A;
        }
        std::vector<Node *> NN = SubLTBits(NI,
                tag + "SubLT" + std::to_string(i),
                _A, Y, B, R[i], curr_dep);
        for (Node * z : NN)
            result.push_back(z);
        curr_dep.clear();
        curr_dep.push_back(result.back()->ntag);
        NN = SelectBits(NI,
                tag + "Sel" + std::to_string(i),
                B, _A, R[i], A, curr_dep);
        for (Node * z : NN)
            result.push_back(z);
        curr_dep.clear();
        curr_dep.push_back(result.back()->ntag);
    }
    // Final copy
    Node * NN = Node::CompNode(NI, tag + "final",
            curr_dep,
            DivBits_final,
            R, Z);
    result.push_back(NN);

    return result;
}
