#ifndef __COMP_MODEL_H__
#define __COMP_MODEL_H__

#include <vector>
#include <variant>
#include <string>
#include <unordered_map>
#include <iostream>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <queue>

#include "networking.h"
#include "group.h"
#include "elgamal.h"

extern unsigned long TRANSCRIPT_SIZE;

typedef std::string DataKey;
typedef std::string NodeTag;

typedef std::variant<int, std::string, Zq_elt, G_elt, Ciphertext, CG_ZKP_1, CG_ZKP_2, Dec_ZKP> Data;

extern std::mutex ctx_mtx;

class GlobalContext {
    public:
        std::unordered_map<DataKey, Data> data;

        static std::string serialize(Data d) {
            if (std::holds_alternative<int>(d)) {
                return std::to_string(std::get<int>(d));
            } else if (std::holds_alternative<std::string>(d)) {
                return std::get<std::string>(d);
            } else if (std::holds_alternative<Zq_elt>(d)) {
                return std::get<Zq_elt>(d).serialize();
            } else if (std::holds_alternative<G_elt>(d)) {
                return std::get<G_elt>(d).serialize();
            } else if (std::holds_alternative<Ciphertext>(d)) {
                return std::get<Ciphertext>(d).serialize();
            } else if (std::holds_alternative<CG_ZKP_1>(d)) {
                return std::get<CG_ZKP_1>(d).serialize();
            } else if (std::holds_alternative<CG_ZKP_2>(d)) {
                return std::get<CG_ZKP_2>(d).serialize();
            } else if (std::holds_alternative<Dec_ZKP>(d)) {
                return std::get<Dec_ZKP>(d).serialize();
            } else {
                assert(0);
            }
        };
        static Data unserialize(std::string s, int index) {
            if (index == 0) {
                return Data { std::in_place_index<0>, std::stoi(s) };
            } else if (index == 1) {
                return Data { std::in_place_index<1>, s };
            } else if (index == 2) {
                Zq_elt z(s.c_str());
                return Data { std::in_place_index<2>, z };
            } else if (index == 3) {
                G_elt z(s.c_str());
                return Data { std::in_place_index<3>, z };
            } else if (index == 4) {
                Ciphertext z(s.c_str());
                return Data { std::in_place_index<4>, z };
            } else if (index == 5) {
                CG_ZKP_1 z(s.c_str());
                return Data { std::in_place_index<5>, z };
            } else if (index == 6) {
                CG_ZKP_2 z(s.c_str());
                return Data { std::in_place_index<6>, z };
            } else if (index == 7) {
                Dec_ZKP z(s.c_str());
                return Data { std::in_place_index<7>, z };
            } else {
                assert (0);
            }
        };

        void set(const DataKey & key, const Data & dat) {
            ctx_mtx.lock();
            data[key] = dat;
            ctx_mtx.unlock();
        }

        Data get(const DataKey & key) {
            ctx_mtx.lock();
            Data d = data[key];
            ctx_mtx.unlock();
            return d;
        }
};

extern GlobalContext Context;

class NodeDAG;

class Node {
    private:
        bool _done;
        bool _running;
        bool _queued;
        bool _send;
        bool _recv;
    public:
        static std::unordered_map<NodeTag, Node> all_nodes;
        static std::unordered_map<std::string, Data> *received_data;

        NodeTag ntag;
        std::string tag;
        std::vector<NodeTag> dependencies;
        std::vector<Node *> fast_dep;  
        std::vector<Node *> anti_dep;  // who depends on me
        void (*code) (std::vector<DataKey>, std::vector<DataKey>);
        std::vector<DataKey> input;
        std::vector<DataKey> output;
        // the following is used only if this is a comm node
        net_info * NI;
        int peer;
        int rank; // to help the scheduling

        static void init(net_info * NI) {
            int n = NI->pub_keys.size();
            received_data = new std::unordered_map<std::string, Data>[n];
        }
        static void clear() {
            delete [] received_data;
        }

        Node () {}

        Node(NodeTag _ntag,
                std::string _tag,
                std::vector<NodeTag> _dependencies,
                void (*_code) (std::vector<DataKey>, std::vector<DataKey>),
                std::vector<DataKey> _input,
                std::vector<DataKey> _output,
                net_info * _NI,
                int _peer
                ) : ntag(_ntag), tag(_tag), dependencies(_dependencies), code(_code), input(_input), output(_output), NI(_NI), peer(_peer)
        {
            _done = false;
            _running = false;
            _queued = false;
            _send = false;
            _recv = false;
            rank = 0;
        }

        static Node * SendNode(net_info * _NI,
                std::string _tag,
                std::vector<NodeTag> _dependencies,
                std::vector<DataKey> _input, int _receiver) {
            std::vector<DataKey> empty_out;
            void (*_code) (std::vector<DataKey>, std::vector<DataKey>) = NULL;
            NodeTag ntag= _tag+"send"+ std::to_string(_NI->me)
                +std::to_string(_receiver);
            Node node = Node(ntag, _tag, _dependencies, _code, _input, empty_out, _NI, _receiver);
            node._send = true;
            node.rank=all_nodes.size();
            assert (all_nodes.find(ntag) == all_nodes.end());
            all_nodes[ntag] = node;
            return &(all_nodes[ntag]);
        }

        static std::vector<Node *> BroadcastNodes(net_info * NI,
                std::string tag, std::vector<NodeTag> dep,
                std::vector<DataKey> input) {
            std::vector<Node *> result;
            for (int i = 0; i < NI->send_sock.size(); ++i) {
                if (i == NI->me)
                    continue;
                result.push_back(Node::SendNode(NI, tag, dep, input, i));
            }
            return result;
        }

        static void recv_loop(const net_info * NI, int peer, NodeDAG *dag);

        static Node * RecvNode(net_info * _NI,
                std::string _tag,
                std::vector<NodeTag> _dependencies,
                std::vector<DataKey> _output, int _sender) {
            std::vector<DataKey> empty_in;
            void (*_code) (std::vector<DataKey>, std::vector<DataKey>) = NULL;
            NodeTag ntag= _tag+"recv"+ std::to_string(_sender)
                + std::to_string(_NI->me);
            Node node = Node(ntag, _tag, _dependencies, _code, empty_in, _output, _NI, _sender);
            node._recv = true;
            node.rank=all_nodes.size();
            assert (all_nodes.find(ntag) == all_nodes.end());
            all_nodes[ntag] = node;
            return &(all_nodes[ntag]);
        }
 
        static Node * CompNode(
                net_info * _NI, // unused
                std::string _tag,
                std::vector<NodeTag> _dependencies,
                void (*_code) (std::vector<DataKey>, std::vector<DataKey>),
                std::vector<DataKey> _input,
                std::vector<DataKey> _output) {

            Node node = Node(_tag, _tag, _dependencies, _code, _input, _output, _NI, 0);
            assert (all_nodes.find(_tag) == all_nodes.end());
            all_nodes[_tag] = node;
            node.rank=all_nodes.size();
            return &(all_nodes[_tag]);
        }

        bool is_done() {
            return _done;
        }
        bool is_running() {
            return _running;
        }
        bool is_queued() {
            return _queued;
        }

        void set_queued() {
            assert (!_queued);
            _queued = true;
        }
        void set_running() {
            assert (!_running);
            _running = true;
        }
        
        bool is_send() {
            return _send;
        }
        
        bool is_recv() {
            return _recv;
        }

        bool is_ready() {
            for (auto & N : fast_dep) {
                if (!(N->is_done()))
                    return false;
            }
            return true;
        }

        bool is_data_ready() {
            assert (is_recv() && is_ready());
            bool got_all = true;
            NI->recv_mutex[peer].lock();
            for (int i = 0; i < output.size(); ++i) {
                std::string tagi = tag + "item" + std::to_string(i);
                if (Node::received_data[peer].find(tagi) == Node::received_data[peer].end()) {
                    got_all = false;
                    break;
                }
            }
            NI->recv_mutex[peer].unlock();
            return got_all;
        }


        static Node * NodeByTag(NodeTag ntag) {
            return &(all_nodes[ntag]);
        }

        static std::vector<Node *> AllNodes() {
            std::vector<Node *> res;
            for (auto & z : all_nodes) {
                res.push_back(&z.second);
            }
            return res;
        }

        void send_run();
        void recv_run();

        void run() {
            assert (is_ready() && is_queued() && is_running());
            assert (_done == false);
            if (is_send()) {
                Node::send_run();
            } else if (is_recv()) {
                Node::recv_run();
            } else {
                code(output, input);
            }
            _done = true;
            _running = false;
        }

        static void static_run(Node *N) {
            N->run();
        }
};

extern std::function<bool (const Node *, const Node *)> NodeRkCmp;

class NodeDAG {
    private:
        size_t number_done;
        std::vector<Node *> ready_recv_nodes;
        std::priority_queue<Node *, std::vector<Node *>,
            std::function<bool (const Node *, const Node *)>>
                ready_send_nodes { NodeRkCmp };
        std::priority_queue<Node *, std::vector<Node *>,
            std::function<bool (const Node *, const Node *)>>
                ready_comp_nodes { NodeRkCmp };
        std::mutex mutex; // for accessing nodes
        std::mutex notify_mutex;
        std::condition_variable notify_cv;
    public:
        NodeDAG() {}

        void init() {
            mutex.lock();
            for (auto & N : Node::AllNodes()) {
                for (auto & n : N->dependencies) {
                    Node::all_nodes[n].anti_dep.push_back(N);
                    (N->fast_dep).push_back(&Node::all_nodes[n]);
                }
                (N->dependencies).clear();
            }
            number_done = 0;

            for (auto & N : Node::AllNodes()) {
                assert (!N->is_done() && !N->is_running() && !N->is_queued());
                if (N->is_ready()) {
                    if (N->is_recv())
                        ready_recv_nodes.push_back(N);
                    else if (N->is_send())
                        ready_send_nodes.push(N);
                    else
                        ready_comp_nodes.push(N);
                    N->set_queued();
                }
            }
            std::cerr << "This computation has " <<
                Node::all_nodes.size() << " nodes in total\n";

            mutex.unlock();
        }

        // Update when node N is finished
        void update(Node * N) {
            assert (N->is_done());
            mutex.lock();
            for (auto & n : N->anti_dep) {
                if (n->is_running() || n->is_done() || n->is_queued())
                    continue;
                if (n->is_ready()) {
                    if (n->is_recv()) {
                        ready_recv_nodes.push_back(n);
                    } else if (n->is_send()) {
                        ready_send_nodes.push(n);
                    } else {
                        ready_comp_nodes.push(n);
                    }
                    n->set_queued();
                }
            }
            mutex.unlock();
        }

        bool all_done() {
            mutex.lock();
            bool res = (number_done == Node::all_nodes.size());
            mutex.unlock();
            return res;
        }

        void wait_update() {
            std::unique_lock<std::mutex> lk(notify_mutex);
            notify_cv.wait(lk);
        }

        void notify_update(Node *N) {
            if (N != NULL)
                update(N);
            notify_cv.notify_all();
        }

        Node * get_comp_task() {
            Node * res = NULL;
            mutex.lock();
            if (!ready_comp_nodes.empty()) {
                res = ready_comp_nodes.top();
                ready_comp_nodes.pop();
                res->set_running();
                number_done++;
            }
            mutex.unlock();
            return res;
        }

        Node * get_send_task() {
            Node * res = NULL;
            mutex.lock();
            if (!ready_send_nodes.empty()) {
                res = ready_send_nodes.top();
                ready_send_nodes.pop();
                res->set_running();
                number_done++;
            }
            mutex.unlock();
            return res;
        }

        Node * get_recv_task() {
            Node * res = NULL;
            mutex.lock();
            for (auto it = ready_recv_nodes.begin(); it != ready_recv_nodes.end(); it++) {
                assert ((*it)->is_recv());
                if (!(*it)->is_data_ready())
                    continue;
                res = (*it);
                res->set_running();
                number_done++;
                ready_recv_nodes.erase(it);
                break;
            }
            mutex.unlock();
            return res;
        }
};

#define SEND    0
#define RECV    1
#define COMP    2
class Worker {
    private:
        int type;
        NodeDAG & dag;

    public:
        Worker(int _type, NodeDAG & _dag) : type(_type), dag(_dag) {
            assert(type == SEND || type == RECV || type == COMP);
        }

        void run() {
            do {
                Node * task = NULL;
                switch (type) {
                    case SEND:
                        task = dag.get_send_task();
                        break;
                    case RECV:
                        task = dag.get_recv_task();
                        break;
                    case COMP:
                        task = dag.get_comp_task();
                        break;
                    default:
                        assert(0);
                }
                if (task == NULL) {
                    dag.wait_update();
                    continue;
                }
                task->run();
                dag.notify_update(task);
            } while (!dag.all_done());
        }
};


#endif   /* __COMP_MODEL_H__ */
