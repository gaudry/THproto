#ifndef __ADD_H__
#define __ADD_H__

#include "comp_model.h"

// X and Y are vectors of encrypted bits.
// Z is the encrypted bits of X+Y.
// By default, the carry-out is forgotten: the result is modulo 2^m,
// where m is the max length of X and Y.
// If Z has length m+1, the carry is put in the last slot.
std::vector<Node *> AddBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, 
        std::vector<DataKey> Y,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep);

// Idem with subtraction. Borrow-out is forgotten.
std::vector<Node *> SubBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, 
        std::vector<DataKey> Y,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep);

// Idem but returns also the encrypted borrow Enc(X<Y).
// If Z is empty, then returns only the borrow.
std::vector<Node *> SubLTBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, 
        std::vector<DataKey> Y,
        std::vector<DataKey> Z,
        DataKey R,
        std::vector<NodeTag> dep);

// Idem but returns also the encrypted borrow Enc(X==Y).
// If Z is empty, then returns only the borrows.
std::vector<Node *> SubLTEqBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, 
        std::vector<DataKey> Y,
        std::vector<DataKey> Z,
        DataKey R, // (X<Y)
        DataKey S, // (X==Y)
        std::vector<NodeTag> dep);

// Aliases, for the case where one wants only to compare X and Y.
static inline
std::vector<Node *> LTBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y,
        DataKey R,
        std::vector<NodeTag> dep) {
    std::vector<DataKey> empty;
    return SubLTBits(NI, tag, X, Y, empty, R, dep);
}

static inline
std::vector<Node *> LTEqBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y,
        DataKey R,
        DataKey S,
        std::vector<NodeTag> dep) {
    std::vector<DataKey> empty;
    return SubLTEqBits(NI, tag, X, Y, empty, R, S, dep);
}

// Z = -X mod 2^m
// X and Z are bit-encoded encrypted integers
std::vector<Node *> NegBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, 
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep);

// Select: Z = (B)?(Y):(X)
std::vector<Node *> Select(net_info *NI, std::string tag,
        DataKey X, DataKey Y, DataKey B,
        DataKey Z,
        std::vector<NodeTag> dep);

std::vector<Node *> SelectBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y, DataKey B,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep);

std::vector<Node *> EqBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y,
        DataKey R,
        std::vector<NodeTag> dep);

#endif   /* __ADD_H__ */
