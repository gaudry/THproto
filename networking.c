#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>
#include <chrono>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cassert>
#include <cstring>
#include <climits>
#include "networking.h"

void my_connect(int &fd, int me, std::string host, int port) {
    int sock;
    fd = 0;
    struct sockaddr_in serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        std::cerr << "Socket creation error\n";
        return;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    if(inet_pton(AF_INET, host.c_str(), &serv_addr.sin_addr)<=0) {
        std::cerr << "Socket creation error\n";
        return;
    }

    while(connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    std::cerr << "Connection to port " << port << " ok\n";
    char hello[10];
    strcpy(hello, "Hello x");
    hello[7] = me + '0';
    send(sock , hello, 8, 0);
    fd = sock;
}

int net_info::init(const char * conf_file, int me) {
    this->me = me;
    std::vector<std::string> hosts;
    std::vector<int> ports;

    std::ifstream infile(conf_file);
    std::string line;
    int N = 0;
    // Get number of trustees
    {
        do {
            std::getline(infile, line);
        } while (line.empty() || line.at(0) == '#');
        std::istringstream iss(line);
        iss.ignore(INT_MAX, '=');
        if (!(iss >> N)) {
            std::cerr << "Error while parsing conf file: Ntrustees\n";
            return 0;
        }
    }
    std::cerr << "Got " << N << " trustees\n";

    // Network info
    while (std::getline(infile, line)) {
        if (line.empty() || line.at(0) == '#')
            continue;
        std::istringstream iss(line);
        std::string host;
        int port;
        if (!(iss >> host >> port)) {
            std::cerr << "Error while parsing conf file: network info\n";
            return 0;
        }
        hosts.push_back(host);
        ports.push_back(port);
        if (hosts.size() == N)
            break;
    }
    std::cerr << "Read successfully network info\n";

    // Public key info
    while (std::getline(infile, line)) {
        if (line.empty() || line.at(0) == '#')
            continue;
        std::istringstream iss(line);
        std::string pk;
        if (!(iss >> pk)) {
            std::cerr << "Error while parsing conf file: public key\n";
            return 0;
        }
        this->pub_keys.push_back(pk);
        if (this->pub_keys.size() == N)
            break;
    }
    std::cerr << "Read successfully public keys\n";


    this->recv_sock.resize(hosts.size(), 0);
    this->send_sock.resize(hosts.size(), 0);

    std::vector<std::thread> threads;
    for (int i = 0; i < hosts.size(); ++i) {
        if (i == me)
            continue;
        std::thread th(my_connect, std::ref(this->send_sock[i]), me, hosts[i], ports[i]);
        threads.push_back(std::move(th));
    }

    int server_fd;
    int opt = 1;
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        std::cerr << "Server socket failed\n";
        return 0;
    }
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
            &opt, sizeof(opt));
    struct sockaddr_in address;
    int addrlen = sizeof(address);
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(ports[me]);
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        std::cerr << "bind failed";
        return 0;
    }
    
    for (int i = 0; i < hosts.size()-1; ++i) {
        int sock;
        if (listen(server_fd, hosts.size()) < 0) {
            perror("listen");
            exit(EXIT_FAILURE);
        }
        if ((sock = accept(server_fd, (struct sockaddr *)&address,
                        (socklen_t*)&addrlen))<0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        char buffer[1024];
        int valread = read(sock, buffer, 1024);
        assert (valread == 8);
        int other = buffer[7]-'0';
        std::cerr << "Accepted from trustee " << other << "\n";
        this->recv_sock[other] = sock;
    }

    for (int i = 0; i < hosts.size()-1; ++i) {
        if (threads[i].joinable())
            threads[i].join();
    }
    std::cerr << "DONE!\n";

    // Allocate mutexes for sockets
    this->send_mutex = new std::mutex[hosts.size()];
    this->recv_mutex = new std::mutex[hosts.size()];

    return 1;
}

