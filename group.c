#include "group.h"
#include "elgamal.h"


int main(){
    Zq_elt a("cb224c09a46a8952da5db63c02e38bdd872b73ca27277753720aa17697575797");
    Zq_elt b("1313e1ff7e98d0178e3674519da101a45e274a513376346e2b571d397060d598");
    std::cout << "a = " << a << "\n";
    std::cout << "b = " << b << "\n";

    G_elt P, Q, PQ, QP, G;
    G = G_elt::base();
    std::cout << "G = " << G << "\n";
    P = G.pow(a);
    std::cout << "G^a = " << P << "\n";
    Q = G.pow(b);
    std::cout << "G^b = " << Q << "\n";
    PQ = P.pow(b);
    QP = Q.pow(a);
    std::cout << "G^ab = " << PQ << "\n";
    std::cout << "G^ba = " << QP << "\n";
}
