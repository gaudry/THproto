#include <cassert>

#include "comp_model.h"
#include "elgamal.h"
#include "cgate.h"
#include "add.h"

// out[0] = in[0]*in[1] / in[2]^2
void AddBits_inner(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 3);
    assert (out.size() == 1);
    Ciphertext R = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext R2 = R*R;
    Context.set(out[0],
            (std::get<Ciphertext>(Context.get(inp[0]))*
            std::get<Ciphertext>(Context.get(inp[1])))/R2);
}

// out[0] = in[0]*in[1] / in[2]^2
// out[1] = sqrt(in[1]*in[3]*in[4]/out[0])
void AddBits_inner2(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 5);
    assert (out.size() == 2);
    Ciphertext R = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext R2 = R*R;
    Ciphertext Z = (std::get<Ciphertext>(Context.get(inp[0]))*
            std::get<Ciphertext>(Context.get(inp[1])))/R2;
    Ciphertext out1 = ((std::get<Ciphertext>(Context.get(inp[1]))*
          std::get<Ciphertext>(Context.get(inp[3]))*
              std::get<Ciphertext>(Context.get(inp[4])))/Z).sqrt();
    Context.set(out[0], Z);
    Context.set(out[1], out1);
}



std::vector<Node *> AddBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, std::vector<DataKey> Y, std::vector<DataKey> Z,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;

    int sX = X.size();
    int sY = Y.size();
    int m = std::max(sX, sY);
    int sZ = Z.size();
    assert (sZ == m || sZ == m+1);
    bool want_cy = (sZ == m+1);
    DataKey R = tag + "R"; // carry
    DataKey A = tag + "A"; // x_i xor y_i
    DataKey T = tag + "T"; // temp

    // R = CSZ(X0, Y0)
    std::vector<NodeTag> curr_dep;
    {
        DataKey _R;
        if ((m == 1) && want_cy)
            _R = Z[1];
        else
            _R = R;
        std::vector<Node *> NN = CGate(NI, tag + "CG0", X[0], Y[0], _R, dep);
        for (Node * z : NN)
            result.push_back(z);
        curr_dep.push_back(result.back()->ntag);
    }
    // Z0 = X0*Y0/R^2
    {
        DataKey _R;
        if ((m == 1) && want_cy)
            _R = Z[1];
        else
            _R = R;
        Node * N = Node::CompNode(NI, tag + "comp0",
                curr_dep,
                AddBits_inner,
                std::vector<DataKey> { X[0], Y[0], _R },
                std::vector<DataKey> { Z[0] });
        result.push_back(N);
        curr_dep.clear();
        curr_dep.push_back(result.back()->ntag);
    }
    // Main loop
    for (int i = 1; i < m; ++i) {
        if (sX > i && sY > i) {
            // A = Xi*Yi / CSZ(Xi,Yi)^2
            std::string tagi = tag + "," + std::to_string(i);
            std::vector<Node *> NN = CGate(NI, tagi + "CG1",
                    X[i], Y[i], T, curr_dep);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            Node * N = Node::CompNode(NI, tagi + "comp1",
                    curr_dep,
                    AddBits_inner,
                    std::vector<DataKey> { X[i], Y[i], T },
                    std::vector<DataKey> { A });
            result.push_back(N);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            // Zi = A*R*CSZ(A,R)^2
            NN = CGate(NI, tagi + "CG2", A, R, T, curr_dep);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            DataKey _R;
            // If this is the last round, put the carry in Z, if wanted
            if ((i == m-1) && (want_cy))
                _R = Z[m];
            else
                _R = R;
            N = Node::CompNode(NI, tagi + "comp2",
                    curr_dep,
                    AddBits_inner2,
                    std::vector<DataKey> { A, R, T, X[i], Y[i] },
                    std::vector<DataKey> { Z[i], _R });
            result.push_back(N);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
        } else {
            // Not implemented
            assert(0);
        }
    }
    return result;
}

// out[0] = in[0]*in[1] / in[2]^2
// out[1] = in[2]*in[3]
void SubBits_inner0(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 4);
    assert (out.size() == 2);
    Ciphertext R = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext R2 = R*R;
    Context.set(out[0],
            (std::get<Ciphertext>(Context.get(inp[0]))*
            std::get<Ciphertext>(Context.get(inp[1])))/R2);
    Context.set(out[1], R*std::get<Ciphertext>(Context.get(inp[3])));
}

void SubBits_precomp(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == out.size());
    for (int i = 0; i < inp.size(); ++i) {
        Context.set(out[i],
                Ciphertext::E1()/std::get<Ciphertext>(Context.get(inp[i])));
    }
}

std::vector<Node *> SubBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, std::vector<DataKey> Y, std::vector<DataKey> Z,
        std::vector<NodeTag> dep)
{
    int me = NI->me;
    std::vector<Node *> result;

    int sX = X.size();
    int sY = Y.size();
    int m = Z.size();
    assert (m == std::max(sX, sY));
    DataKey R = tag + "R"; // borrow
    DataKey A = tag + "A"; // accumulator
    DataKey T = tag + "T"; // temp
    std::vector<DataKey> notY;
    for (int i = 0; i < Y.size(); ++i)
        notY.push_back(tag + "notY" + std::to_string(i));

    std::vector<NodeTag> curr_dep;
    // Precomp (1-Yi) for all i
    {
        std::vector<DataKey> in (Y);
        std::vector<DataKey> out (notY);
        Node * N = Node::CompNode(NI, tag + "precomp", dep,
                SubBits_precomp, in, out);
        result.push_back(N);
        curr_dep.push_back(result.back()->ntag);
    }
    // A = CSZ(X0, Y0)
    {
        std::vector<Node *> NN = CGate(NI, tag + "CG0", X[0], Y[0], A, dep);
        for (Node * z : NN)
            result.push_back(z);
        curr_dep.push_back(result.back()->ntag);
    }
    // Z0 = X0*Y0/A^2
    // R = A*NotY0
    {
        Node * N = Node::CompNode(NI, tag + "comp0",
                curr_dep,
                SubBits_inner0,
                std::vector<DataKey> { X[0], Y[0], A, notY[0] },
                std::vector<DataKey> { Z[0], R });
        result.push_back(N);
        curr_dep.clear();
        curr_dep.push_back(result.back()->ntag);
    }
    // Main loop
    for (int i = 1; i < m; ++i) {
        if (sX > i && sY > i) {
            // A = Xi*notYi / CSZ(Xi,notYi)^2
            std::string tagi = tag + std::to_string(i);
            std::vector<Node *> NN = CGate(NI, tagi + "CG1",
                    X[i], notY[i], T, curr_dep);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            Node * N = Node::CompNode(NI, tagi + "comp1",
                    curr_dep,
                    AddBits_inner,
                    std::vector<DataKey> { X[i], notY[i], T },
                    std::vector<DataKey> { A });
            result.push_back(N);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            // Zi = A*R*CSZ(A,R)^2
            NN = CGate(NI, tagi + "CG2", A, R, T, curr_dep);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            N = Node::CompNode(NI, tagi + "comp2",
                    curr_dep,
                    AddBits_inner2,
                    std::vector<DataKey> { A, R, T, X[i], notY[i] },
                    std::vector<DataKey> { Z[i], R });
            result.push_back(N);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
        } else {
            // Not implemented
            assert(0);
        }
    }
    return result;
}

// out[0] = in[0]*in[1] / in[2]^2
// out[1] = in[1]/in[2]
void SubLTBits_inner0(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 3);
    assert (out.size() == 2);
    Ciphertext A = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext A2 = A*A;
    if (out[0] != "NULL") {
        Context.set(out[0],
                (std::get<Ciphertext>(Context.get(inp[0]))*
                 std::get<Ciphertext>(Context.get(inp[1])))/A2);
    }
    Context.set(out[1], std::get<Ciphertext>(Context.get(inp[1]))/A);
}

// out[0] = in[0]*in[1] / in[2]^2
// out[1] = in[3]*in[5]/(in[4]*in[2])
void SubLTBits_inner2(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 6);
    assert (out.size() == 2);
    Ciphertext R = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext R2 = R*R;
    if (out[0] != "NULL") {
        Ciphertext Z = (std::get<Ciphertext>(Context.get(inp[0]))*
                std::get<Ciphertext>(Context.get(inp[1])))/R2;
        Context.set(out[0], Z);
    }
    Ciphertext out1 = 
        std::get<Ciphertext>(Context.get(inp[3]))*
        std::get<Ciphertext>(Context.get(inp[5])) /
        ( std::get<Ciphertext>(Context.get(inp[2]))*
          std::get<Ciphertext>(Context.get(inp[4])) );
    Context.set(out[1], out1);
}

std::vector<Node *> SubLTBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, std::vector<DataKey> Y, std::vector<DataKey> Z,
        DataKey RR, std::vector<NodeTag> dep)
{
    int me = NI->me;
    std::vector<Node *> result;

    int sX = X.size();
    int sY = Y.size();
    int m = std::max(sX, sY);
    // If Z is empty, only the borrow-out is wanted.
    if (Z.size() == 0) {
        for (int i = 0; i < m; ++i) {
            Z.push_back("NULL");
        }
    }
    assert (m == std::max(sX, sY));
    DataKey R = tag + "R"; // borrow
    DataKey A = tag + "A"; // accumulator A
    DataKey B = tag + "B"; // accumulator B
    DataKey C = tag + "C"; // accumulator C

    std::vector<NodeTag> curr_dep;
    // A = CSZ(X0, Y0)
    {
        std::vector<Node *> NN = CGate(NI, tag + "CG0", X[0], Y[0], A, dep);
        for (Node * z : NN)
            result.push_back(z);
        curr_dep.push_back(result.back()->ntag);
    }
    // Z0 = X0*Y0/A^2
    // R = Y0/A
    {
        DataKey _R;
        if (m == 1)
            _R = RR;
        else
            _R = R;
        Node * N = Node::CompNode(NI, tag + "comp0",
                curr_dep,
                SubLTBits_inner0,
                std::vector<DataKey> { X[0], Y[0], A},
                std::vector<DataKey> { Z[0], _R });
        result.push_back(N);
        curr_dep.clear();
        curr_dep.push_back(result.back()->ntag);
    }
    // Main loop
    for (int i = 1; i < m; ++i) {
        if (sX > i && sY > i) {
            // A = CSZ(Yi,R)
            // B = Yi*R/A^2
            std::string tagi = tag + std::to_string(i);
            std::vector<Node *> NN = CGate(NI, tagi + "CG1",
                    Y[i], R, A, curr_dep);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            Node * N = Node::CompNode(NI, tagi + "comp1",
                    curr_dep,
                    AddBits_inner,
                    std::vector<DataKey> { Y[i], R, A },
                    std::vector<DataKey> { B });
            result.push_back(N);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            // C = CSZ(Xi, B)
            // Zi = Xi*B/C^2
            // R = Yi/(A*C)
            NN = CGate(NI, tagi + "CG2", X[i], B, C, curr_dep);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
            DataKey _R = (i == m-1)?RR:R;
            N = Node::CompNode(NI, tagi + "comp2",
                    curr_dep,
                    SubLTBits_inner2,
                    std::vector<DataKey> { X[i], B, C, Y[i], A, R},
                    std::vector<DataKey> { Z[i], _R });
            result.push_back(N);
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
        } else {
            // Not implemented
            assert(0);
        }
    }
    return result;
}


void Sel_step0(std::vector<DataKey> out, std::vector<DataKey> inp) {
    Context.set(out[0],
            std::get<Ciphertext>(Context.get(inp[1]))/
             std::get<Ciphertext>(Context.get(inp[0])));
}

void Sel_step1(std::vector<DataKey> out, std::vector<DataKey> inp) {
    Context.set(out[0],
            std::get<Ciphertext>(Context.get(inp[0]))*
             std::get<Ciphertext>(Context.get(inp[1])));
}

std::vector<Node *> Select(net_info *NI, std::string tag,
        DataKey X, DataKey Y, DataKey B,
        DataKey Z,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    Node * N = Node::CompNode(NI, tag + "comp0",
            dep,
            Sel_step0,
            std::vector<DataKey> { X, Y },
            std::vector<DataKey> { tag + "tmp0" });
    result.push_back(N);
    std::vector<NodeTag> dep0 = { N->ntag };
    std::vector<Node *> NN = CGate(NI, tag + "CG",
            tag + "tmp0", B, tag + "tmp1", dep0);
    for (Node * z : NN)
        result.push_back(z);
    dep0.clear();
    dep0.push_back(result.back()->ntag);
    Node * NNN = Node::CompNode(NI, tag + "comp1",
            dep0,
            Sel_step1,
            std::vector<DataKey> { X, tag + "tmp1" },
            std::vector<DataKey> { Z });
    result.push_back(NNN);
    return result;
}

void SelB_step0(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (2*out.size() == inp.size());
    for (int i = 0; i < out.size(); ++i) {
        Context.set(out[i],
                std::get<Ciphertext>(Context.get(inp[2*i+1]))/
                std::get<Ciphertext>(Context.get(inp[2*i])));
    }
}

void SelB_step1(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (2*out.size() == inp.size());
    for (int i = 0; i < out.size(); ++i) {
        Context.set(out[i],
                std::get<Ciphertext>(Context.get(inp[2*i+1]))*
                std::get<Ciphertext>(Context.get(inp[2*i])));
    }
}



std::vector<Node *> SelectBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y, DataKey B,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep)
{
    assert (X.size() == Y.size());
    assert (X.size() == Z.size());
    std::vector<DataKey> tmp;
    for (int i = 0; i < X.size(); ++i)
        tmp.push_back(tag + "tmp" + std::to_string(i));

    std::vector<Node *> result;
    std::vector<DataKey> in;
    for (int i = 0; i < X.size(); ++i) {
        in.push_back(X[i]);
        in.push_back(Y[i]);
    }
    Node * N = Node::CompNode(NI, tag + "comp0",
            dep, SelB_step0, in, tmp);
    result.push_back(N);
    std::vector<NodeTag> dep0 = { N->ntag };
    std::vector<NodeTag> dep1;
    for (int i = 0; i < X.size(); ++i) {
        std::vector<Node *> NN = CGate(NI, tag + "CG" + std::to_string(i),
                tmp[i], B, tmp[i], dep0);
        for (Node * z : NN)
            result.push_back(z);
        dep1.push_back(result.back()->ntag);
    }
    in.clear();
    for (int i = 0; i < X.size(); ++i) {
        in.push_back(X[i]);
        in.push_back(tmp[i]);
    }
    Node * NNN = Node::CompNode(NI, tag + "comp1",
            dep1, SelB_step1, in, Z);
    result.push_back(NNN);
    return result;
}


void Neg_init(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 2);
    assert (out.size() == 3);
    Ciphertext X0 = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext X1 = std::get<Ciphertext>(Context.get(inp[1]));
    Context.set(out[0], X0);
    Context.set(out[1], Ciphertext::E1()/X0);
    Context.set(out[2], Ciphertext::E1()/X1);
}

void Neg_comp(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 4);
    assert (out.size() == 3);
    Ciphertext Xi = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext R0 = std::get<Ciphertext>(Context.get(inp[1]));
    Ciphertext R1 = std::get<Ciphertext>(Context.get(inp[2]));
    Context.set(out[0], Xi*R0/(R1*R1));
    Context.set(out[1], R1);
    if (inp[3] != "NULL") {
        Context.set(out[2], Ciphertext::E1()/
                std::get<Ciphertext>(Context.get(inp[3])));
    }
}

std::vector<Node *> NegBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int m = X.size();
    assert (m == Z.size());

    DataKey R0 = tag + "R0";
    DataKey R1 = tag + "R1";
    DataKey notXi = tag + "NotXi";

    Node * N = Node::CompNode(NI, tag + "init",
            dep, Neg_init,
            std::vector<DataKey> { X[0], X[1] },
            std::vector<DataKey> { Z[0], R0, notXi });
    result.push_back(N);
    std::vector<NodeTag> curr_dep = {N->ntag};

    for (int i = 1; i < m; ++i) {
        std::vector<Node *> NN = CGate(NI, tag + "CG" + std::to_string(i),
                notXi, R0, R1, curr_dep);
        for (Node * z : NN)
            result.push_back(z);
        curr_dep.clear();
        curr_dep.push_back(result.back()->ntag);
        DataKey _XX;
        if (i == m-1) {
            _XX = "NULL";
        } else {
            _XX = X[i+1];
        }
        Node * N = Node::CompNode(NI, tag + "comp" + std::to_string(i),
            curr_dep, Neg_comp,
            std::vector<DataKey> { notXi, R0, R1, _XX },
            std::vector<DataKey> { Z[i], R0, notXi });
        result.push_back(N);
        curr_dep.push_back(N->ntag);
    }
    return result;
}

void Eq_comp(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 3);
    assert (out.size() == 1);
    Ciphertext Xi = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext Yi = std::get<Ciphertext>(Context.get(inp[1]));
    Ciphertext Ai = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext Res = Ciphertext::E1()*Ai*Ai/(Xi*Yi);
    Context.set(out[0], Res);
}

std::vector<Node *> EqBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y,
        DataKey R,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int m = X.size();
    assert (m == Y.size());

    std::vector<DataKey> A;
    for (int i = 0; i < m; ++i) {
        A.push_back(tag + "A" + std::to_string(i));
    }

    std::vector<NodeTag> final_dep;
    for (int i = 0; i < m; ++i) {
        std::vector<Node *> NN = CGate(NI, tag + "CG" + std::to_string(i),
                X[i], Y[i], A[i], dep);
        for (Node * z : NN)
            result.push_back(z);
        std::vector<NodeTag> depi = { result.back()->ntag };
        Node * N = Node::CompNode(NI, tag + "comp" + std::to_string(i),
            depi, Eq_comp,
            std::vector<DataKey> { X[i], Y[i], A[i] },
            std::vector<DataKey> { A[i] });
        result.push_back(N);
        final_dep.push_back(N->ntag);
    }
    std::vector<Node *> NN = CSZAcc(NI, tag + "CGAcc", A, R, final_dep);
    for (Node * z : NN)
        result.push_back(z);
    return result;
}


std::vector<Node *> SubLTEqBits(net_info *NI, std::string tag,
        std::vector<DataKey> X, std::vector<DataKey> Y,
        std::vector<DataKey> Z, // (X-Y) mod 2^m
        DataKey R, // (X<Y)
        DataKey S, // (X==Y)
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    
    std::vector<Node *> NN = SubLTBits(NI, tag + "lt",
            X, Y, Z, R, dep);
    for (Node * z : NN)
        result.push_back(z);
    // put a dep here, that could be avoided...
    std::vector<NodeTag> dep0 = { result.back()->ntag };
    NN = EqBits(NI, tag + "eq", X, Y, S, dep0);
    for (Node * z : NN)
        result.push_back(z);
    return result;
}

 
