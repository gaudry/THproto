#ifndef __NETWORKING_H__
#define __NETWORKING_H__

#include <vector>
#include <string>
#include <thread>
#include <mutex>

class net_info {

    public:
        // My trustee number
        int me;
        // The sockets with other trustees
        //   *_sock[me] is undefined
        std::vector<int> recv_sock;
        std::vector<int> send_sock;

        std::mutex* send_mutex; 
        std::mutex* recv_mutex; 

        // Public keys of everyone
        std::vector<std::string> pub_keys;

        // Initialization:
        //   - connects to others to create snd_sock
        //   - creates and accepts others on rcv_sock
        // At the end, if success, all TCP connections are open and
        // ready for read/write.
        // The integer "me" tells which trustee I am.
        int init(const char * conf_file, int me);
        void close() {
            delete send_mutex;
            delete recv_mutex;
        }
};

#endif   /* __NETWORKING_H__ */
