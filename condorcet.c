#include <cassert>
#include <chrono>

#include "comp_model.h"
#include "elgamal.h"
#include "cgate.h"
#include "add.h"
#include "arith.h"
#include "condorcet.h"

void PrefMatrix_diag(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 0);
    for (int i = 0; i < out.size(); ++i)
        Context.set(out[i], Ciphertext::E0());
}

// Z = 1-X  (can be in-place)
void Comp_Not(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == out.size());
    for (int i = 0; i < inp.size(); ++i) {
        Ciphertext X = std::get<Ciphertext>(Context.get(inp[i]));
        Context.set(out[i], Ciphertext::E1()/X);
    }
}

void Comp_cpy(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == out.size());
    for (int i = 0; i < out.size(); ++i)
        Context.set(out[i], std::get<Ciphertext>(Context.get(inp[i])));
}

// Preference matrix. See condorcet.h for description.
std::vector<Node *> PrefMatrix(net_info *NI, std::string tag,
        std::vector<std::vector<DataKey>> B,
        std::vector<std::vector<DataKey>> M,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int k = M.size();
    assert (B.size() == k);
    int l = B[0].size();

    std::vector<NodeTag> curr_dep;

    std::vector<std::vector<DataKey>> C;
    std::vector<std::vector<DataKey>> T;
    for (int i = 0; i < k; ++i) {
        C.push_back(std::vector<DataKey> { });
        T.push_back(std::vector<DataKey> { });
        std::string tagCi = tag + "C" + std::to_string(i) + ",";
        std::string tagTi = tag + "T" + std::to_string(i) + ",";
        for (int j = 0; j < k; ++j) {
            C[i].push_back(tagCi + std::to_string(j));
            T[i].push_back(tagTi + std::to_string(j));
        }
    }

    for (int i = 0; i < k; ++i) {
        std::string tagi = tag + "," + std::to_string(i) + ",";
        for (int j = i+1; j < k; ++j) {
            std::vector<Node *> NN = LTEqBits(NI,
                    tagi + std::to_string(j) + "," + "LTEq",
                    B[i], B[j],
                    M[i][j], C[i][j],
                    dep);
            for (Node * z : NN)
                result.push_back(z);
            std::vector<NodeTag> dep0 = { result.back()->ntag };
            Node * N = Node::CompNode(NI, tagi + "not" + std::to_string(j),
                dep0,
                Comp_Not,
                std::vector<DataKey> { M[i][j], C[i][j] },
                std::vector<DataKey> { T[i][j], C[i][j] });
            result.push_back(N);
            dep0.clear();
            dep0.push_back(result.back()->ntag);
            NN = CGate(NI,
                    tagi + "CG" + std::to_string(j) + ",",
                    T[i][j], C[i][j],
                    M[j][i],
                    dep0);
            for (Node * z : NN)
                result.push_back(z);
            curr_dep.push_back(result.back()->ntag);
        }
    }

    std::vector<DataKey> Out;
    for (int i = 0; i < k; ++i)
        Out.push_back(M[i][i]);
    Node * N = Node::CompNode(NI, tag + "diag",
                curr_dep,
                PrefMatrix_diag,
                std::vector<DataKey> {  },
                Out);
    result.push_back(N);

    return result;
}


// Adjacency matrix. See condorcet.h for description.
std::vector<Node *> AdjMatrix(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> M,
        std::vector<std::vector<std::vector<DataKey>>> A,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int k = M.size();
    int l = M[0][0].size();
    assert (A.size() == k);
    assert (A[0][0].size() == l);

    // Let's parallelize only on j, not on i.
    std::vector<std::vector<DataKey>> D;
    std::vector<DataKey> N;
    std::vector<DataKey> notN;
    for (int i = 0; i < k; ++i) {
        N.push_back(tag + "N" + std::to_string(i));
        notN.push_back(tag + "notN" + std::to_string(i));
        D.push_back(std::vector<DataKey> { });
        std::string tagDi = tag + "D" + std::to_string(i) + ",";
        for (int j = 0; j < l; ++j) {
            D[i].push_back(tagDi + std::to_string(j));
        }
    }

    std::vector<NodeTag> dep_loop = dep; // one loop after the other.
    for (int i = 0; i < k; ++i) {
        std::string tagi = tag + std::to_string(i) + ",";
        std::vector <NodeTag> newdep;
        for (int j = i+1; j < k; ++j) {
            // SubLT
            std::vector<Node *> NN = SubLTBits(NI,
                    tagi + "SubLT" + std::to_string(j) + ",",
                    M[i][j], M[j][i],
                    D[j], N[j],
                    dep_loop);
            for (Node * z : NN)
                result.push_back(z);
            std::vector<NodeTag> dep0 = { result.back()->ntag };
            // Not N
            Node * NNN = Node::CompNode(NI, tagi + "notN" + std::to_string(j),
                dep0,
                Comp_Not,
                std::vector<DataKey> { N[j] },
                std::vector<DataKey> { notN[j] });
            result.push_back(NNN);
            dep0.clear();
            dep0.push_back(result.back()->ntag);
            // CGate1
            NN = CSZBits(NI,
                    tagi + "CG1," + std::to_string(j) + ",",
                    D[j], notN[j],
                    A[i][j],
                    dep0);
            for (Node * z : NN)
                result.push_back(z);
            dep0.clear();
            dep0.push_back(result.back()->ntag);
            // Neg D
            NN = NegBits(NI,
                    tagi + "neg" + std::to_string(j) + ",",
                    D[j], D[j], dep0);
            for (Node * z : NN)
                result.push_back(z);
            dep0.clear();
            dep0.push_back(result.back()->ntag);
            // CGate2
            NN = CSZBits(NI,
                    tagi + "CG2," + std::to_string(j) + ",",
                    D[j], N[j],
                    A[j][i],
                    dep0);
            for (Node * z : NN)
                result.push_back(z);
            newdep.push_back(result.back()->ntag);
        }
        if (i != k-1)
            dep_loop = newdep;
    }

    std::vector<DataKey> Out;
    for (int i = 0; i < k; ++i) {
        for (int j = 0; j < l; ++j) {
            Out.push_back(A[i][i][j]);
        }
    }
    Node * NNN = Node::CompNode(NI, tag + "diag",
                dep_loop,
                PrefMatrix_diag,
                std::vector<DataKey> {  },
                Out);
    result.push_back(NNN);
    return result;
}

// Floyd-Warshall
// In-place: this destroys S.
std::vector<Node *> FloydWarshall(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> S,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int n = S.size();
    assert (S[0].size() == n);
    int l = S[0][0].size(); // bit-size of matrix entries

    // Initialize names for all the temp variables.
    std::vector<std::vector<DataKey>> LT;
    std::vector<std::vector<std::vector<DataKey>>> A;
    std::vector<std::vector<std::vector<DataKey>>> B;
    for (int i = 0; i < n; ++i) {
        LT.push_back(std::vector<DataKey> { });
        for (int j = 0; j < n; ++j) {
            LT[i].push_back(tag + "LT" + std::to_string(i)
                    + "," + std::to_string(j));
        }
    }
    for (int i = 0; i < n; ++i) {
        A.push_back(std::vector<std::vector<DataKey>> { });
        B.push_back(std::vector<std::vector<DataKey>> { });
        for (int j = 0; j < n; ++j) {
            A[i].push_back(std::vector<DataKey> { });
            B[i].push_back(std::vector<DataKey> { });
            for (int s = 0; s < l; ++s) {
                A[i][j].push_back(tag + "A" +
                        std::to_string(i) + "," +
                        std::to_string(j) + "," +
                        std::to_string(s));
                B[i][j].push_back(tag + "B" +
                        std::to_string(i) + "," +
                        std::to_string(j) + "," +
                        std::to_string(s));
            }
        }
    }

    std::vector<NodeTag> dep_loop = dep;
    for (int k = 0; k < n; ++k) {
        std::vector<NodeTag> newdep;
        // Compute B
        for (int i = 0; i < n; ++i) {
            std::string tagki = tag + std::to_string(k) + "," +
                std::to_string(i);
            for (int j = 0; j < n; ++j) {
                if (i == j)
                    continue;
                // First LT
                std::vector<Node *> NN = LTBits(NI,
                        tagki + "LT1," + std::to_string(j) + ",",
                        S[i][k], S[k][j], LT[i][j],
                        dep_loop);
                for (Node * z : NN)
                    result.push_back(z);
                std::vector<NodeTag> dep0 = { result.back()->ntag };
                // First Select
                NN = SelectBits(NI,
                        tagki + "Sel1," + std::to_string(j) + ",",
                        S[k][j], S[i][k], LT[i][j],
                        A[i][j], dep0);
                for (Node * z : NN)
                    result.push_back(z);
                dep0.clear();
                dep0.push_back(result.back()->ntag);
                // Second LT
                NN = LTBits(NI,
                        tagki + "LT2," + std::to_string(j) + ",",
                        S[i][j], A[i][j], LT[i][j],
                        dep0);
                for (Node * z : NN)
                    result.push_back(z);
                dep0.clear();
                dep0.push_back(result.back()->ntag);
                // Second Select
                NN = SelectBits(NI,
                        tagki + "Sel2," + std::to_string(j) + ",",
                        S[i][j], A[i][j], LT[i][j],
                        B[i][j], dep0);
                for (Node * z : NN)
                    result.push_back(z);
                newdep.push_back(result.back()->ntag);
            }
        }
        dep_loop = newdep;
        // Copy B to S
        std::vector<DataKey> In;
        std::vector<DataKey> Out;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < n; ++j) {
                if (i == j)
                    continue;
                for (int s = 0; s < l; ++s) {
                    In.push_back(B[i][j][s]);
                    Out.push_back(S[i][j][s]);
                }
            }
        }
        Node * N = Node::CompNode(NI, tag + std::to_string(k) + "copyB",
                dep_loop,
                Comp_cpy,
                In,
                Out);
        result.push_back(N);
        dep_loop.clear();
        dep_loop.push_back(result.back()->ntag);
    }
    return result;
}

void debug(std::vector<DataKey> out, std::vector<DataKey> inp) {
    for (int i = 0; i < inp.size(); ++i) {
        std::cerr << "tag " << inp[i] << " = " <<
            std::get<Ciphertext>(Context.get(inp[i])).serialize() <<
            "\n";
    }
}

void log_msg(std::vector<DataKey> out, std::vector<DataKey> inp) {
    static auto save_time = std::chrono::system_clock::now();
    auto now = std::chrono::system_clock::now();
    std::cerr << std::get<std::string>(Context.get(inp[0])) 
        << " in "
        << std::chrono::duration_cast<std::chrono::milliseconds>(now-save_time).count()/1000. << " sec\n";
    std::cerr << "  up to here, performed " << COUNT_POW << " scal. mult.\n";
    save_time = now;
}


std::vector<Node *> CondorcetSchulze(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> B,
        std::vector<DataKey> Winner,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int n = B.size();       // n is the number of voters
    int k = B[0].size();    // k is the number of candidates
    int l = B[0][0].size(); // l is the number of bits of the grades
    assert (Winner.size() == k);
    // Compute size of integers in the aggregated pref matrix
    int m = 1;
    while (n > ((1<<m) - 1))
        m++;

    /////////////////////////////////
    // Initialize temporary variables
    std::vector<std::vector<std::vector<DataKey>>> M;
    for (int p = 0; p < n; ++p) {
        M.push_back(std::vector<std::vector<DataKey>> { });
        for (int i = 0; i < k; ++i) {
            M[p].push_back(std::vector<DataKey> { });
            for (int j = 0; j < k; ++j) {
                M[p][i].push_back(tag + "M" +
                        std::to_string(p) + "," +
                        std::to_string(i) + "," +
                        std::to_string(j));
            }
        }
    }
    std::vector<std::vector<std::vector<DataKey>>> Magg;
    for (int i = 0; i < k; ++i) {
        Magg.push_back(std::vector<std::vector<DataKey>> { });
        for (int j = 0; j < k; ++j) {
            Magg[i].push_back(std::vector<DataKey> { });
            for (int s = 0; s < m; ++s) {
                Magg[i][j].push_back(tag + "Magg" +
                        std::to_string(i) + "," +
                        std::to_string(j) + "," +
                        std::to_string(s));
            }
        }
    }
    // we can reuse the aggregated matrix for adj matrix
    std::vector<std::vector<std::vector<DataKey>>> Adj = Magg;

    std::vector<std::vector<DataKey>> W;
    for (int i = 0; i < k; ++i) {
        W.push_back(std::vector<DataKey> { });
        for (int j = 0; j < k; ++j) {
            W[i].push_back(tag + "W" + std::to_string(i) + 
                    "," + std::to_string(j));
        }
    }
    std::vector<DataKey> w;
    for (int i = 0; i < k; ++i) {
        w.push_back(tag + "w" + std::to_string(i));
    }

    /////////////////////////////////
    // Start of description of computation

    std::vector<NodeTag> depinit;
    {
        Context.set("msg0", std::string("Starting MPC computation"));
        Node * N = Node::CompNode(NI, tag + "log0", dep, log_msg,
                std::vector<DataKey> { "msg0" },
                std::vector<DataKey> { });
        result.push_back(N);
        depinit.clear();
        depinit.push_back(result.back()->ntag);
    }

    // Compute preference matrices
    std::vector<NodeTag> dep0;
    {
        std::vector<NodeTag> curr_dep = depinit;
        std::vector<NodeTag> prev_dep = depinit;
        std::string tagpref = tag + "pref";
        for (int p = 0; p < n; ++p) {
            std::vector<Node *> NN = PrefMatrix(NI,
                    tagpref + std::to_string(p),
                    B[p], M[p], prev_dep);
            for (Node * z : NN)
                result.push_back(z);
            dep0.push_back(result.back()->ntag);
            prev_dep = curr_dep;
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
        }
    }

    {
        Context.set("msg1", std::string("Finished PrefMatrix"));
        Node * N = Node::CompNode(NI, tag + "log1", dep0, log_msg,
                std::vector<DataKey> { "msg1" },
                std::vector<DataKey> { });
        result.push_back(N);
        dep0.clear();
        dep0.push_back(result.back()->ntag);
    }

    // Compute the aggregated pref matrix
    std::vector<NodeTag> dep1;
    {
        std::vector<NodeTag> curr_dep = dep0;
        std::vector<NodeTag> prev_dep = dep0;
        std::string tagagg = tag + "agg";
        for (int i = 0; i < k; ++i) {
            for (int j = 0; j < k; ++j) {
                std::vector<DataKey> In;
                for (int p = 0; p < n; ++p) {
                    In.push_back(M[p][i][j]);
                }
                std::vector<Node *> NN = Aggreg(NI,
                        tagagg + std::to_string(i) + "," + std::to_string(j),
                        In,
                        Magg[i][j], prev_dep);
                for (Node * z : NN)
                    result.push_back(z);
                dep1.push_back(result.back()->ntag);
                prev_dep = curr_dep;
                curr_dep.clear();
                curr_dep.push_back(result.back()->ntag);
            }
        }
    }

    {
        Context.set("msg2", std::string("Finished Aggregation of Pref matrices"));
        Node * N = Node::CompNode(NI, tag + "log2", dep1, log_msg,
                std::vector<DataKey> { "msg2" },
                std::vector<DataKey> { });
        result.push_back(N);
        dep1.clear();
        dep1.push_back(result.back()->ntag);
    }


    // Compute the adjacency matrix
    std::vector<NodeTag> dep2;
    {
        std::vector<Node *> NN = AdjMatrix(NI,
                tag + "adj",
                Magg,
                Adj,
                dep1);
        for (Node * z : NN)
            result.push_back(z);
        dep2.push_back(result.back()->ntag);
    }

    {
        Context.set("msg3", std::string("Finished Adjacency Matrix"));
        Node * N = Node::CompNode(NI, tag + "log3", dep2, log_msg,
                std::vector<DataKey> { "msg3" },
                std::vector<DataKey> { });
        result.push_back(N);
        dep2.clear();
        dep2.push_back(result.back()->ntag);
    }

    // Run Floyd-Warshall (in-place)
    std::vector<NodeTag> dep3;
    {
        std::vector<Node *> NN = FloydWarshall(NI,
                tag + "FW",
                Adj, dep2);
        for (Node * z : NN)
            result.push_back(z);
        dep3.push_back(result.back()->ntag);
    }

    {
        Context.set("msg4", std::string("Finished Floyd-Warshall"));
        Node * N = Node::CompNode(NI, tag + "log4", dep3, log_msg,
                std::vector<DataKey> { "msg4" },
                std::vector<DataKey> { });
        result.push_back(N);
        dep3.clear();
        dep3.push_back(result.back()->ntag);
    }

    // Deduce the winners
    std::vector<NodeTag> final_dep;
    {
        for (int i = 0; i < k; ++i) {
            std::vector<NodeTag> dep4;
            for (int j = 0; j < k; ++j) {
                std::vector<Node *> NN = LTBits(NI,
                        tag + "finLT" + std::to_string(i) + "," + std::to_string(j) + ",",
                        Adj[i][j], Adj[j][i],
                        W[i][j], dep3);
                for (Node * z : NN)
                    result.push_back(z);
                dep4.push_back(result.back()->ntag);
            }
            Node * N = Node::CompNode(NI,
                    tag + "finNot" + std::to_string(i),
                    dep4,
                    Comp_Not,
                    W[i], W[i]);
            result.push_back(N);
            dep4.clear();
            dep4.push_back(result.back()->ntag);
            std::vector<Node *> NN = CSZAcc(NI,
                    tag + "finCSZ" + std::to_string(i) + ",",
                    W[i], w[i], dep4);
            for (Node * z : NN)
                result.push_back(z);
            dep4.clear();
            dep4.push_back(result.back()->ntag);
            NN = Dec(NI,
                    tag + "Dec" + std::to_string(i) + ",",
                    w[i], Winner[i], dep4);
            for (Node * z : NN)
                result.push_back(z);
            final_dep.push_back(result.back()->ntag);
        }
    }

    {
        Context.set("msg5", std::string("Finished Deducing winners"));
        Node * N = Node::CompNode(NI, tag + "log5", final_dep, log_msg,
                std::vector<DataKey> { "msg5" },
                std::vector<DataKey> { });
        result.push_back(N);
    }


    return result;
}

std::vector<Node *> PreAgglomerate(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> B,
        std::vector<std::vector<std::vector<DataKey>>> Magg,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int n = B.size();       // n is the number of voters
    int k = B[0].size();    // k is the number of candidates
    int l = B[0][0].size(); // l is the number of bits of the grades
    assert (Magg.size() == k);
    // Compute size of integers in the aggregated pref matrix
    int m = 1;
    while (n > ((1<<m) - 1))
        m++;
    assert (Magg[0][0].size() == m);

    /////////////////////////////////
    // Initialize temporary variables
    std::vector<std::vector<std::vector<DataKey>>> M;
    for (int p = 0; p < n; ++p) {
        M.push_back(std::vector<std::vector<DataKey>> { });
        for (int i = 0; i < k; ++i) {
            M[p].push_back(std::vector<DataKey> { });
            for (int j = 0; j < k; ++j) {
                M[p][i].push_back(tag + "M" +
                        std::to_string(p) + "," +
                        std::to_string(i) + "," +
                        std::to_string(j));
            }
        }
    }

    /////////////////////////////////
    // Start of description of computation

    std::vector<NodeTag> depinit;
    {
        Context.set("msg0", std::string("Starting MPC computation"));
        Node * N = Node::CompNode(NI, tag + "log0", dep, log_msg,
                std::vector<DataKey> { "msg0" },
                std::vector<DataKey> { });
        result.push_back(N);
        depinit.clear();
        depinit.push_back(result.back()->ntag);
    }

    // Compute preference matrices
    std::vector<NodeTag> dep0;
    {
        std::vector<NodeTag> curr_dep = depinit;
        std::vector<NodeTag> prev_dep = depinit;
        std::string tagpref = tag + "pref";
        for (int p = 0; p < n; ++p) {
            std::vector<Node *> NN = PrefMatrix(NI,
                    tagpref + std::to_string(p),
                    B[p], M[p], prev_dep);
            for (Node * z : NN)
                result.push_back(z);
            dep0.push_back(result.back()->ntag);
            prev_dep = curr_dep;
            curr_dep.clear();
            curr_dep.push_back(result.back()->ntag);
        }
    }

    {
        Context.set("msg1", std::string("Finished PrefMatrix"));
        Node * N = Node::CompNode(NI, tag + "log1", dep0, log_msg,
                std::vector<DataKey> { "msg1" },
                std::vector<DataKey> { });
        result.push_back(N);
        dep0.clear();
        dep0.push_back(result.back()->ntag);
    }

    // Compute the aggregated pref matrix
    std::vector<NodeTag> dep1;
    {
        std::vector<NodeTag> curr_dep = dep0;
        std::vector<NodeTag> prev_dep = dep0;
        std::string tagagg = tag + "agg";
        for (int i = 0; i < k; ++i) {
            for (int j = 0; j < k; ++j) {
                std::vector<DataKey> In;
                for (int p = 0; p < n; ++p) {
                    In.push_back(M[p][i][j]);
                }
                std::vector<Node *> NN = Aggreg(NI,
                        tagagg + std::to_string(i) + "," + std::to_string(j),
                        In,
                        Magg[i][j], prev_dep);
                for (Node * z : NN)
                    result.push_back(z);
                dep1.push_back(result.back()->ntag);
                prev_dep = curr_dep;
                curr_dep.clear();
                curr_dep.push_back(result.back()->ntag);
            }
        }
    }

    {
        Context.set("msg2", std::string("Finished Aggregation of Pref matrices"));
        Node * N = Node::CompNode(NI, tag + "log2", dep1, log_msg,
                std::vector<DataKey> { "msg2" },
                std::vector<DataKey> { });
        result.push_back(N);
        dep1.clear();
        dep1.push_back(result.back()->ntag);
    }

    return result;
}

std::vector<Node *> PostAgglomerate(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<std::vector<DataKey>>>> Magg_list,
        std::vector<std::vector<std::vector<DataKey>>> Magg,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int n = Magg_list.size();       // n is the number of matrices
    int k = Magg_list[0].size();    // k is the number of candidates
    int m = Magg_list[0][0].size(); // m is the number of bits in input
    assert (Magg.size() == k);
    int m2 = Magg[0][0].size();     // m2 is the number of bits in output

    std::vector<NodeTag> depinit;
    {
        Context.set("msg0", std::string("Starting MPC computation"));
        Node * N = Node::CompNode(NI, tag + "log0", dep, log_msg,
                std::vector<DataKey> { "msg0" },
                std::vector<DataKey> { });
        result.push_back(N);
        depinit.clear();
        depinit.push_back(result.back()->ntag);
    }


    // Compute the aggregated pref matrix
    std::vector<NodeTag> dep1;
    {
        std::vector<NodeTag> curr_dep = depinit;
        std::vector<NodeTag> prev_dep = depinit;
        std::string tagagg = tag + "agg";
        for (int i = 0; i < k; ++i) {
            for (int j = 0; j < k; ++j) {
                std::vector<std::vector<DataKey>> In;
                for (int p = 0; p < n; ++p) {
                    In.push_back(Magg_list[p][i][j]);
                }
                std::vector<Node *> NN = AggregBits(NI,
                        tagagg + std::to_string(i) + "," + std::to_string(j),
                        In,
                        Magg[i][j], prev_dep);
                for (Node * z : NN)
                    result.push_back(z);
                dep1.push_back(result.back()->ntag);
                prev_dep = curr_dep;
                curr_dep.clear();
                curr_dep.push_back(result.back()->ntag);
            }
        }
    }

    {
        Context.set("msg2", std::string("Finished Aggregation of Pref matrices"));
        Node * N = Node::CompNode(NI, tag + "log2", dep1, log_msg,
                std::vector<DataKey> { "msg2" },
                std::vector<DataKey> { });
        result.push_back(N);
    }
    return result;
}

std::vector<Node *> FinishCondorcet(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> Magg,
        std::vector<DataKey> Winner,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int k = Magg.size();
    assert (Winner.size() == k);

    // we can reuse the aggregated matrix for adj matrix
    std::vector<std::vector<std::vector<DataKey>>> Adj = Magg;

    std::vector<std::vector<DataKey>> W;
    for (int i = 0; i < k; ++i) {
        W.push_back(std::vector<DataKey> { });
        for (int j = 0; j < k; ++j) {
            W[i].push_back(tag + "W" + std::to_string(i) + 
                    "," + std::to_string(j));
        }
    }
    std::vector<DataKey> w;
    for (int i = 0; i < k; ++i) {
        w.push_back(tag + "w" + std::to_string(i));
    }
 
    std::vector<NodeTag> depinit;
    {
        Context.set("msg0", std::string("Starting MPC computation"));
        Node * N = Node::CompNode(NI, tag + "log0", dep, log_msg,
                std::vector<DataKey> { "msg0" },
                std::vector<DataKey> { });
        result.push_back(N);
        depinit.clear();
        depinit.push_back(result.back()->ntag);
    }


    // Compute the adjacency matrix
    std::vector<NodeTag> dep2;
    {
        std::vector<Node *> NN = AdjMatrix(NI,
                tag + "adj",
                Magg,
                Adj,
                depinit);
        for (Node * z : NN)
            result.push_back(z);
        dep2.push_back(result.back()->ntag);
    }

    {
        Context.set("msg3", std::string("Finished Adjacency Matrix"));
        Node * N = Node::CompNode(NI, tag + "log3", dep2, log_msg,
                std::vector<DataKey> { "msg3" },
                std::vector<DataKey> { });
        result.push_back(N);
        dep2.clear();
        dep2.push_back(result.back()->ntag);
    }

    // Run Floyd-Warshall (in-place)
    std::vector<NodeTag> dep3;
    {
        std::vector<Node *> NN = FloydWarshall(NI,
                tag + "FW",
                Adj, dep2);
        for (Node * z : NN)
            result.push_back(z);
        dep3.push_back(result.back()->ntag);
    }

    {
        Context.set("msg4", std::string("Finished Floyd-Warshall"));
        Node * N = Node::CompNode(NI, tag + "log4", dep3, log_msg,
                std::vector<DataKey> { "msg4" },
                std::vector<DataKey> { });
        result.push_back(N);
        dep3.clear();
        dep3.push_back(result.back()->ntag);
    }

    // Deduce the winners
    std::vector<NodeTag> final_dep;
    {
        for (int i = 0; i < k; ++i) {
            std::vector<NodeTag> dep4;
            for (int j = 0; j < k; ++j) {
                std::vector<Node *> NN = LTBits(NI,
                        tag + "finLT" + std::to_string(i) + "," + std::to_string(j) + ",",
                        Adj[i][j], Adj[j][i],
                        W[i][j], dep3);
                for (Node * z : NN)
                    result.push_back(z);
                dep4.push_back(result.back()->ntag);
            }
            Node * N = Node::CompNode(NI,
                    tag + "finNot" + std::to_string(i),
                    dep4,
                    Comp_Not,
                    W[i], W[i]);
            result.push_back(N);
            dep4.clear();
            dep4.push_back(result.back()->ntag);
            std::vector<Node *> NN = CSZAcc(NI,
                    tag + "finCSZ" + std::to_string(i) + ",",
                    W[i], w[i], dep4);
            for (Node * z : NN)
                result.push_back(z);
            dep4.clear();
            dep4.push_back(result.back()->ntag);
            NN = Dec(NI,
                    tag + "Dec" + std::to_string(i) + ",",
                    w[i], Winner[i], dep4);
            for (Node * z : NN)
                result.push_back(z);
            final_dep.push_back(result.back()->ntag);
        }
    }

    {
        Context.set("msg5", std::string("Finished Deducing winners"));
        Node * N = Node::CompNode(NI, tag + "log5", final_dep, log_msg,
                std::vector<DataKey> { "msg5" },
                std::vector<DataKey> { });
        result.push_back(N);
    }

    return result;
}
