#ifndef __ARITH_H__
#define __ARITH_H__

#include "comp_model.h"

// Input: X is a vector of encrypted bits
// Output: Z is the encrypted sum of all bits, bitencoded.
// Z must have length 1 + Floor(log_2(X.size()))
std::vector<Node *> Aggreg(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep);

// Input: X is a vector of encrypted bit-encoded integers
// Output: Z is the encrypted sum of all integers, bitencoded.
// Z must have length X[0].size() + log_2(X.size())
std::vector<Node *> AggregBits(net_info *NI, std::string tag,
        std::vector<std::vector<DataKey>> X,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep);


// Input: X and Y, two encrypted integers, bit-encoded
// Output: Z, is the encrypted product, bit-encoded
std::vector<Node *> MulBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep);

// Input: X and Y, two encrypted integers, bit-encoded
// Output: Z, is the encrypted division, bit-encoded
// Assume X<Y.
// The output is the r binary digits of x/y, in reverse order: z0 is the
// lsb.
std::vector<Node *> DivBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        std::vector<DataKey> Y,
        std::vector<DataKey> Z,
        std::vector<NodeTag> dep);

#endif   /* __ARITH_H__ */
