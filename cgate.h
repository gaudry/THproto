#ifndef __CGATE_H__
#define __CGATE_H__

#include "comp_model.h"

std::vector<Node *> CGate(net_info *NI, std::string tag, DataKey X, DataKey Y,
        DataKey Z, std::vector<NodeTag> dep);

std::vector<Node *> Dec(net_info *NI, std::string tag, DataKey X, 
        DataKey Z, std::vector<NodeTag> dep);

// Compute the product of all the bits
std::vector<Node *> CSZAcc(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        DataKey Y,
        std::vector<NodeTag> dep);

// Parallel CSZ, with a common control bit Y.
std::vector<Node *> CSZBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        DataKey Y,
        std::vector<DataKey> Z, std::vector<NodeTag> dep);
#endif   /* __CGATE_H__ */
