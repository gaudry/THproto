# Implementation of the Condorcet-Schulze following the MPC algorithm,
# but with clear-texts, in order to check its correctness.

from numpy import zeros
from numpy import copy

## Primitives
def zero_matrix(k):
    return zeros((k,k),int)

def SubLt(x,y):
    return x-y,x<y

def SubLtEq(x,y):
    return x-y,x<y,x==y

def Not(b):
    return 1-b

def CSZ(x,b):
    if b:
        return x
    else:
        return 0

def Aggreg(list):
    return sum(list)

def Neg(x):
    return -x

def Lt(a,b):
    return a<b

def Select(x,y,b):
    return y if b else x

## Fonctions

def ballot_to_matrix(ballot):
    '''ballot: list of integers
    line 2 to 7 of Algorithm 48'''
    k = len(ballot)
    Mp = zero_matrix(k)
    for i in range(k):
        for j in range(i+1,k):
            _,T,C = SubLtEq(ballot[i],ballot[j])
            Mp[i,j]= 1 if T else 0
            Mp[j,i]= CSZ(Not(T),Not(C))
        # Mp[i,i] = 0 (already done by zeros function)
    return Mp

def positive_to_adjacency(matrix):
    '''matrix: square numpy array of positive integers
    line 11 to 16 of algorithm 48'''
    k = len(matrix)
    A = zero_matrix(k)
    for i in range(k):
        for j in range(k):
            D,N = SubLt(matrix[i,j],matrix[j,i])
            F = Neg(D)
            A[i,j] = CSZ(D,Not(N))
            A[j,i] = CSZ(F,N)
    return A

def FloydWarshall(P):
    ''' A: square numpy array of positive integers'''
    n = len(P)
    S = copy(P)
    A = zero_matrix(n)
    B = zero_matrix(n)
    for k in range(n):
        for i in range(n):
            for j in range(n):
                if i != j:
                    A[i,j] = Select(S[k,j],S[i,k],Lt(S[i,k],S[k,j]))
                    B[i,j] = Select(S[i,j],A[i,j],Lt(S[i,j],A[i,j]))
        for i in range(n):
            for j in range(n):
                if i != j:
                    S[i,j] = B[i,j]
    return S

def winners_ind(S):
    ''' S : square numpy array of positive integers'''
    k = len(S)
    W = zero_matrix(k)
    for i in range(k):
        for j in range(k):
            W[i,j] = Not(Lt(S[i,j],S[j,i]))
    return [(sum(W[i,:])==k) for i in range(k)]

def winners_name(winners,names):
    return [names[w] for w in range(len(winners)) if winners[w]]

## Algorithm

def CondorcetSchulze(ballots):
    '''ballots: list of ballot of the same size k
        ballot: list of positive integers'''
    M = []
    for ballot in ballots:
        M.append(ballot_to_matrix(ballot))
    Ma = Aggreg(M)
    A = positive_to_adjacency(Ma)
    S = FloydWarshall(A)
    return winners_ind(S)

## Unitary tests

from numpy import shape
from numpy import array_equal
from random import randint

# test ballot to matrix

def random_rank(k):
    return randint(0,(1<<k.bit_length())-1)

def random_ballot(k):
    return [random_rank(k) for j in range(k)]

def isCorrect(matrix,rank):
    '''matrix: square numpy array of positive integers
        rank : list of positive integers (same size)'''
    k = len(rank)
    (x,y)=shape(matrix)
    if x != y:
        print("matrix not square")
        return False
    if x != k:
        print("incorrect matrix size")
        return False
    for i in range(k):
        for j in range(k):
            if (matrix[i,j] == 1) and (rank[i] >= rank[j]):
                return False
            if (matrix[i,j] == 0) and (rank[i] < rank[j]):
                return False
            if (rank[i] < rank[j]) and (matrix[i,j] != 1):
                return False
            if (rank[i] >= rank[j]) and (matrix[i,j] != 0):
                return False
    return True

def test_ballot_to_matrix(k):
    for i in range(10000):
        ballot = random_ballot(k)
        matrix = ballot_to_matrix(ballot)
        if not isCorrect(matrix,ballot):
            print(ballot)
            return False
    return True

def testb2m():
    for k in range(2,32):
        if not test_ballot_to_matrix(k):
            return False
    return True

# test positive to adjacency

def true_adjacency(M):
    k = len(M)
    A = zero_matrix(k)
    for i in range(k):
        for j in range(k):
            A[i,j] = M[i,j]-M[j,i]
            if A[i,j] < 0:
                A[i,j] = 0
    return A

def test_positive_to_adjacency(k):
    for j in range(100):
        M  = zero_matrix(k)
        for i in range(100):
            ballot = [random_rank(k) for j in range(k)]
            M += ballot_to_matrix(ballot)
        vraie = true_adjacency(M)
        A = positive_to_adjacency(M)
        if not array_equal(A,vraie):
            return False
    return True

def testp2a():
    for k in range(2,32):
        if not test_positive_to_adjacency(k):
            return False
    return True

# Test FloydWarshall

def all_paths(A):
    n = len(A)
    paths = []
    for i in range(n):
        pathsfromi = [[[A[i,j]]] if A[i,j]>0 else [] for j in range(n)]
        for length in range(2,n):
            for j in range(n):
                for k in range(n):
                    if A[j,k] > 0:
                        pathsfromi[k] += [x+[A[j,k]] for x in pathsfromi[j]]
        paths.append(pathsfromi)
    return paths

def strength(path):
    return min(path)

def force(A):
    paths = all_paths(A)
    k = len(A)
    f = zero_matrix(k)
    for i in range(k):
        for j in range(k):
            if paths[i][j] == []:
                f[i,j] = 0
            else:
                f[i,j] = max([strength(sigma) for sigma in paths[i][j]])
    return f

def test_FW(k):
    for i in range(100):
        M = zero_matrix(k)
        for j in range(1000):
            M += ballot_to_matrix([random_rank(k) for i in range(k)])
        A = positive_to_adjacency(M)
        S = FloydWarshall(A)
        F = force(A)
        for i in range(k):
            for j in range(k):
                if (i != j) and S[i,j] != F[i,j]:
                    return False # There could be differences on the diagonal
    return True

def testfw():
    for k in range(2,16):
        if not test_FW(k):
            return False
    return True

## Test Condorcet_winner

def condorcet_winner(ballots):
    M = sum([ballot_to_matrix(b) for b in ballots])
    k = len(M)
    for i in range(k):
        g = True
        for j in range(k):
            if (j != i) and M[i,j]<=M[j,i]:
                g = False
        if g:
            return i
    return None

def test_cw():
    for n in range(7,1000):
        for k in range(2,32):
            ballots = [random_ballot(k) for i in range(n)]
            res = condorcet_winner(ballots)
            if (res != None):
                c = CondorcetSchulze(ballots)
                if (sum(c)!=1) and (c[res]!=1):
                    return False
    return True
