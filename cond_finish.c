#include <iostream>
#include <cassert>
#include <cstring>
#include <thread>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "networking.h"
#include "group.h"
#include "elgamal.h"
#include "comp_model.h"
#include "cgate.h"
#include "add.h"
#include "arith.h"
#include "condorcet.h"

GlobalContext Context;
std::mutex ctx_mtx;
unsigned long COUNT_POW = 0;

void send_worker(NodeDAG * nodeDag) {
    Worker wk(SEND, *nodeDag);
    wk.run();
}

void recv_worker(NodeDAG * nodeDag) {
    Worker wk(RECV, *nodeDag);
    wk.run();
}

void comp_worker(NodeDAG * nodeDag) {
    Worker wk(COMP, *nodeDag);
    wk.run();
}

std::vector<DataKey> DeclareName(std::string prefix, int k) {
    std::vector<DataKey> out;
    for (int i = 0; i < k; ++i) {
        DataKey name = prefix + std::to_string(i);
        out.push_back(name);
    }
    return out;
}

std::vector<std::vector<std::vector<DataKey>>> ReadMatrix(
        std::string pref, int k, int m) {
    std::vector<std::vector<std::vector<DataKey>>> Magg;
    for (int i = 0; i < k; ++i) {
        Magg.push_back(std::vector<std::vector<DataKey>> { });
        for (int j = 0; j < k; ++j) {
            Magg[i].push_back(std::vector<DataKey> { });
            for (int s = 0; s < m; ++s) {
                std::string name = pref +
                    std::to_string(i) + "," +
                    std::to_string(j) + "," +
                    std::to_string(s);
                Magg[i][j].push_back(name);
                std::string line;
                std::getline(std::cin, line);
                Ciphertext c(line.c_str());
                Context.data[name] = c;
            }
        }
    }
    return Magg;
}


// Usage:
//   ./finish_cond 0 sk 4 6 < aggregated_matrix_file
// where:
//   0  is the number of the trustee (same order as in the conf file)
//   sk is the public key of the trustee
//   4  is the number of candidates
//   6  is the width of the matrix entry
//   aggregated_matrix_file contains the output of post_agg
int main(int argc, char *argv[]) {
    assert (argc == 5);
    net_info NI;
    NI.init("conf", atoi(argv[1]));
    Node::init(&NI);

    Zq_elt sk(argv[2]);
    G_elt mypk = G_elt::base().pow(sk);
    // Put my secret key and the general public key in global context
    {
        G_elt pk;
        for (int i = 0; i < NI.pub_keys.size(); ++i) {
            G_elt zz(NI.pub_keys[i].c_str());
            Context.data["pk" + std::to_string(i)] = zz;
            if (i == NI.me)
                assert(zz == mypk);
            pk = pk*zz;
        }
        Context.data["pk"] = pk;
        Context.data["sk"] = sk;
        Context.data["mypk"] = mypk;
        Context.data["me"] = NI.me;
        std::cerr << "Main pk is " << pk << "\n";
    }

    int k = atoi(argv[3]);
    int m = atoi(argv[4]);

    // Read matrix
    std::vector<std::vector<std::vector<DataKey>>> Magg = 
        ReadMatrix("MagList", k, m);

    // Winner
    std::vector<DataKey> Winner = DeclareName("Winner", k);
    
    //////////////////////////////////////
    // Declare the DAG of MPC computation.
    NodeDAG nodeDag;
    std::vector<Node *> Cond = FinishCondorcet(&NI, "PostAgglomerate",
            Magg,
            Winner,
            std::vector<NodeTag> { });
    nodeDag.init();

    ///////////////////////
    // Start worker threads
    for (int i = 0; i < NI.pub_keys.size(); ++i) {
        if (i == NI.me)
            continue;
        std::thread th_recvloop(Node::recv_loop, &NI, i, &nodeDag);
        th_recvloop.detach();
    }
    std::thread th_send(send_worker, &nodeDag);
    std::thread th_recv(recv_worker, &nodeDag);
    std::thread th_comp(comp_worker, &nodeDag);
    std::thread th_comp2(comp_worker, &nodeDag);
    std::thread th_comp3(comp_worker, &nodeDag);
    std::thread th_comp4(comp_worker, &nodeDag);

    // Wait for the MPC computation to finish
    th_send.join();
    th_recv.join();
    th_comp.join();
    th_comp2.join();
    th_comp3.join();
    th_comp4.join();

    ///////////////
    // Print result
    std::cerr << "Condorcet-Schulze winners are: ";
    for (int i = 0; i < Winner.size(); ++i) {
        std::cerr << std::get<G_elt>(Context.data[Winner[i]]).dlog() << " ";
    }
    std::cerr << "\n";


    std::cerr << "Performed " << COUNT_POW << " scalar mult\n";
    std::cerr << "Transcript size is " << TRANSCRIPT_SIZE << " Zq/G elements\n";
    return EXIT_SUCCESS;
}
