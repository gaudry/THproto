#ifndef __ELGAMAL_H__
#define __ELGAMAL_H__

#include <vector>
#include "group.h"

class Ciphertext {
    private:
    public:
        G_elt c1;
        G_elt c2;

        static const int size = 2*G_elt::size;

        // h is the public key
        // m must be small (typical value is 0 / 1)
        Ciphertext(G_elt h, int m) {
            Zq_elt r = Zq_elt::random();
            G_elt G = G_elt::base();
            c1 = G.pow(r);
            if (m == 0) {
                c2 = h.pow(r);
                return;
            }
            int sign = 0;
            if (m < 0) {
                sign = 1;
                m = -m;
            }
            G_elt Gm = G;
            for (int i = 1; i < m; ++i)
                Gm = Gm*G;
            if (sign)
                Gm = Gm.invert();
            c2 = Gm*h.pow(r);
        }

        // Same, but with given randomness
        Ciphertext(G_elt h, int m, Zq_elt r) {
            G_elt G = G_elt::base();
            c1 = G.pow(r);
            if (m == 0) {
                c2 = h.pow(r);
                return;
            }
            int sign = 0;
            if (m < 0) {
                sign = 1;
                m = -m;
            }
            G_elt Gm = G;
            for (int i = 1; i < m; ++i)
                Gm = Gm*G;
            if (sign)
                Gm = Gm.invert();
            c2 = Gm*h.pow(r);
        }

        Ciphertext(const char * str) {
            G_elt _c1(str);
            G_elt _c2(str + 2*crypto_core_ed25519_BYTES);
            c1 = _c1;
            c2 = _c2;
        }

        Ciphertext() { }

        // Some constant encryptions with no randomness
        static Ciphertext E0() {
            Ciphertext C;
            // c1 and c2 initialized with neutral by default: ok.
            return C;
        }
        static Ciphertext E1() {
            Ciphertext C;
            // c1 initialized with neutral by default: ok.
            C.c2 = G_elt::base();
            return C;
        }
        static Ciphertext Eminus1() {
            Ciphertext C;
            // c1 initialized with neutral by default: ok.
            C.c2 = G_elt::base().invert();
            return C;
        }


        Ciphertext ReEnc(G_elt h, Zq_elt &r) {
            Ciphertext CC;
            r = Zq_elt::random();
            G_elt gr = G_elt::base().pow(r);
            G_elt hr = h.pow(r);
            CC.c1 = c1*gr;
            CC.c2 = c2*hr;
            return CC;
        }

        Ciphertext invert() {
            Ciphertext CC;
            CC.c1 = c1.invert();
            CC.c2 = c2.invert();
            return CC;
        }

        Ciphertext pow(int k) {
            int sign = 0;
            if (k < 0) {
                k = -k;
                sign = 1;
            }
            Zq_elt K(k);
            Ciphertext CC;
            CC.c1 = c1.pow(K);
            CC.c2 = c2.pow(K);
            if (sign)
                return CC.invert();
            else
                return CC;
        }

        Ciphertext pow(Zq_elt const& k) {
            Ciphertext CC;
            CC.c1 = c1.pow(k);
            CC.c2 = c2.pow(k);
            return CC;
        }

        Ciphertext sqrt() {
            Zq_elt k = Zq_elt::qp1s2();
            Ciphertext CC;
            CC.c1 = c1.pow(k);
            CC.c2 = c2.pow(k);
            return CC;
        }

        Ciphertext operator*(Ciphertext const&b) {
            Ciphertext CC;
            CC.c1 = c1*b.c1;
            CC.c2 = c2*b.c2;
            return CC;
        }
        Ciphertext operator/(Ciphertext const&b) {
            Ciphertext CC;
            CC.c1 = c1/b.c1;
            CC.c2 = c2/b.c2;
            return CC;
        }
        
        bool operator==(Ciphertext const& b) const {
            return (c1 == b.c1) && (c2 == b.c2);
        }

        static G_elt decrypt(Ciphertext const& C, Zq_elt const& sk) {
            return C.c2 / C.c1.pow(sk);
        }

        static G_elt partial_decrypt(Ciphertext const& C, Zq_elt const& sk) {
            return C.c1.pow(sk);
        }

        static G_elt combine_partial_decrypt(Ciphertext const& C,
                std::vector<G_elt> const & part) {
            G_elt acc;
            for (auto & z : part) {
                acc = acc*z;
            }
            return C.c2/acc;
        }
        
        static int decrypt_to_int(Ciphertext const& C, Zq_elt const& sk) {
            G_elt P = decrypt(C, sk);
            return P.dlog();
        }

        std::string serialize() const {
            return c1.serialize() + c2.serialize();
        }
};

class ZKP01 {
    public:
        Ciphertext e0;
        Ciphertext e1;
        Zq_elt sigma0;
        Zq_elt rho0;
        Zq_elt sigma1;
        Zq_elt rho1;

        static const int size = 2*Ciphertext::size + 4*Zq_elt::size;

        ZKP01() {}

        ZKP01(Ciphertext _e0, Ciphertext _e1,
                Zq_elt _sigma0, Zq_elt _rho0,
                Zq_elt _sigma1, Zq_elt _rho1) :
            e0(_e0), e1(_e1),
            sigma0(_sigma0), rho0(_rho0),
            sigma1(_sigma1), rho1(_rho1) {}

        ZKP01(const char *str) {
            e0 = Ciphertext(str); str += 4*crypto_core_ed25519_BYTES;
            e1 = Ciphertext(str); str += 4*crypto_core_ed25519_BYTES;
            sigma0 = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            rho0 = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            sigma1 = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            rho1 = Zq_elt(str);
        }

        std::string serialize() const {
            return e0.serialize() + e1.serialize() +
                sigma0.serialize() + rho0.serialize() +
                sigma1.serialize() + rho1.serialize();
        }
};


class Dec_ZKP {
    public:
        G_elt e1;
        G_elt e2;
        Zq_elt ans;
        
        static const int size = 2*G_elt::size + Zq_elt::size;

        Dec_ZKP(G_elt _e1, G_elt _e2, Zq_elt _ans) :
            e1(_e1), e2(_e2), ans(_ans) {}

        Dec_ZKP(const char *str) {
            e1 = G_elt(str); str += 2*crypto_core_ed25519_BYTES;
            e2 = G_elt(str); str += 2*crypto_core_ed25519_BYTES;
            ans = Zq_elt(str);
        }

        std::string serialize() const {
            return e1.serialize() + e2.serialize() + ans.serialize();
        }
};



class CG_ZKP_1 {

    private:
    public:
        Ciphertext eX0;
        Ciphertext eY0;
        Ciphertext eX1;
        Ciphertext eY1;
        Zq_elt sigma0;
        Zq_elt sigma1;
        Zq_elt rhoX0;
        Zq_elt rhoX1;
        Zq_elt rhoY0;
        Zq_elt rhoY1;
        
        static const int size = 4*Ciphertext::size + 6*Zq_elt::size;

        CG_ZKP_1(Ciphertext _eX0, Ciphertext _eY0,
                Ciphertext _eX1, Ciphertext _eY1,
                Zq_elt _sigma0, Zq_elt _sigma1,
                Zq_elt _rhoX0, Zq_elt _rhoY0,
                Zq_elt _rhoX1, Zq_elt _rhoY1
                ) :
            eX0(_eX0), eY0(_eY0), eX1(_eX1), eY1(_eY1),
            sigma0(_sigma0), sigma1(_sigma1),
            rhoX0(_rhoX0), rhoY0(_rhoY0),
            rhoX1(_rhoX1), rhoY1(_rhoY1) {}

        CG_ZKP_1(const char *str) {
            eX0 = Ciphertext(str); str += 4*crypto_core_ed25519_BYTES;
            eY0 = Ciphertext(str); str += 4*crypto_core_ed25519_BYTES;
            eX1 = Ciphertext(str); str += 4*crypto_core_ed25519_BYTES;
            eY1 = Ciphertext(str); str += 4*crypto_core_ed25519_BYTES;
            sigma0 = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            sigma1 = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            rhoX0 = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            rhoX1 = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            rhoY0 = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            rhoY1 = Zq_elt(str);
        }
        
        std::string serialize() const {
            return eX0.serialize() + eY0.serialize() +
                eX1.serialize() + eY1.serialize() +
                sigma0.serialize() + sigma1.serialize() +
                rhoX0.serialize() + rhoX1.serialize() +
                rhoY0.serialize() + rhoY1.serialize();
        }
};

class CG_ZKP_2 {

    private:
    public:
        Ciphertext eX;
        Zq_elt aX;
        Ciphertext eY;
        Zq_elt aY;
        
        static const int size = 2*Ciphertext::size + 2*Zq_elt::size;

        CG_ZKP_2(Ciphertext _eX, Zq_elt _aX,
                Ciphertext _eY, Zq_elt _aY) :
            eX(_eX), eY(_eY), 
            aX(_aX), aY(_aY) {}

        CG_ZKP_2(const char *str) {
            eX = Ciphertext(str); str += 4*crypto_core_ed25519_BYTES;
            aX = Zq_elt(str); str += 2*crypto_core_ed25519_SCALARBYTES;
            eY = Ciphertext(str); str += 4*crypto_core_ed25519_BYTES;
            aY = Zq_elt(str);
        }
        
        std::string serialize() const {
            return eX.serialize() + aX.serialize() +
                eY.serialize() + aY.serialize();
        }
};




#endif   /* __ELGAMAL_H__ */
