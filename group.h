#ifndef __GROUP_H__
#define __GROUP_H__

#include <iostream>
#include <sodium.h>
#include <cassert>

extern unsigned long COUNT_POW;

static inline void to_hex(char &c0, char &c1, const unsigned char c) {
        c0 = c & 0xf;
        c1 = c >> 4;
        if (c0 <= 9)
            c0 += '0';
        else
            c0 += 'a' - 10;
        if (c1 <= 9)
            c1 += '0';
        else
            c1 += 'a' - 10;
}

class Zq_elt {
    private:
        unsigned char k[crypto_core_ed25519_SCALARBYTES];
    public:
        friend class G_elt;

        static const int size = 1;

        static Zq_elt random() {
            Zq_elt c;
            crypto_core_ed25519_scalar_random(c.k);
            return c;
        }

        static Zq_elt qp1s2() {
            return Zq_elt("f7e97a2e8d31092c6bce7b51ef7c6f0a00000000000000000000000000000008");
        }

        Zq_elt() {
            for (int i = 0; i < crypto_core_ed25519_SCALARBYTES; ++i)
                k[i] = 0;
        }
 
        Zq_elt(unsigned int m) {
            k[0] = m & 255;
            k[1] = (m >> 8) & 255;
            k[2] = (m >> 16) & 255;
            k[3] = (m >> 24) & 255;
            for (int i = 4; i < crypto_core_ed25519_SCALARBYTES; ++i)
                k[i] = 0;

        }
       
        Zq_elt(const char * str) {
            for (int i = 0; i < crypto_core_ed25519_SCALARBYTES; ++i)
                k[i] = 0;
            for (unsigned int i = 0; i < 2*crypto_core_ed25519_SCALARBYTES; ++i) {
                if (str[i] == '\0')
                    return;
                unsigned char c;
                if (str[i] >= 'a')
                    c = str[i]-'a'+10;
                else
                    c = str[i]-'0';
                if (!(i & 1U)) {
                    c <<= 4;
                }
                k[i / 2] |= c;
            }
        }

        Zq_elt operator-() const {
            Zq_elt c;
            crypto_core_ed25519_scalar_negate(c.k, k);
            return c;
        }

        Zq_elt operator-(Zq_elt const& b) const {
            Zq_elt c;
            crypto_core_ed25519_scalar_sub(c.k, k, b.k);
            return c;
        }

        Zq_elt operator+(Zq_elt const& b) const {
            Zq_elt c;
            crypto_core_ed25519_scalar_add(c.k, k, b.k);
            return c;
        }

        Zq_elt operator*(Zq_elt const& b) const {
            Zq_elt c;
            crypto_core_ed25519_scalar_mul(c.k, k, b.k);
            return c;
        }
        bool operator==(Zq_elt const& b) const {
            for (int i = 0; i < crypto_core_ed25519_SCALARBYTES; ++i) {
                if (b.k[i] != k[i])
                    return false;
            }
            return true;
        }

        Zq_elt invert() const {
            Zq_elt c;
            int ret = crypto_core_ed25519_scalar_invert(c.k, k);
            assert (ret == 0);
            return c;
        }

        // Use twice sha256 to create a 512-bit integer and reduce it mod L.
        static Zq_elt hash(std::string const & s) {
            unsigned char tmp[crypto_core_ed25519_NONREDUCEDSCALARBYTES];
            assert (crypto_core_ed25519_NONREDUCEDSCALARBYTES==2*crypto_hash_sha256_BYTES);
            std::string s0 = "0"+s;
            std::string s1 = "1"+s;
            crypto_hash_sha256(tmp, (const unsigned char*)s0.c_str(), s0.length());
            crypto_hash_sha256(tmp+crypto_hash_sha256_BYTES,
                    (const unsigned char*)s1.c_str(), s1.length());
            Zq_elt c;
            crypto_core_ed25519_scalar_reduce(c.k, tmp);
            return c;
        }

        std::string serialize() const {
            std::string s;
            for (int i = 0; i < crypto_core_ed25519_SCALARBYTES; ++i) {
                unsigned char c = k[i];
                char c0, c1;
                to_hex(c0, c1, c);
                s = (s + c1) + c0;
            }
            return s;
        }
        friend std::ostream& operator<<(std::ostream& os, const Zq_elt& k);

};

inline std::ostream& operator<<(std::ostream& os, const Zq_elt& k) {
    for (int i = 0; i < crypto_core_ed25519_SCALARBYTES; ++i) {
        unsigned char c = k.k[i];
        char c0, c1;
        to_hex(c0, c1, c);
        os << c1 << c0;
    }
    return os;
}

class G_elt {
    private:
        unsigned char bytes[crypto_core_ed25519_BYTES];
    public:

        static const int size = 1;

        // g = 0x5866666666666666666666666666666666666666666666666666666666666666
        // (as bytes)
        static G_elt base() {
            G_elt x;
            x.bytes[0] = 0x58; // Warning, msb vs lsb, here
            for (int i = 1; i < crypto_core_ed25519_BYTES; ++i)
                x.bytes[i] = 0x66;
            return x;
        }

        // initialize to group neutral
        G_elt() {
            bytes[0] = 0x01;
            for (int i = 1; i < crypto_core_ed25519_BYTES; ++i)
                bytes[i] = 0;
        }

        G_elt(const char * str) {
            for (int i = 0; i < crypto_core_ed25519_BYTES; ++i)
                bytes[i] = 0;
            for (unsigned int i = 0; i < 2*crypto_core_ed25519_BYTES; ++i) {
                if (str[i] == '\0')
                    return;
                unsigned char c;
                if (str[i] >= 'a')
                    c = str[i]-'a'+10;
                else
                    c = str[i]-'0';
                if (!(i & 1U)) {
                    c <<= 4;
                }
                bytes[i / 2] |= c;
            }
        }

        int is_neutral() const {
            if (bytes[0] != 0x01)
                return 0;
            for (int i = 1; i < crypto_core_ed25519_BYTES; ++i)
                if (bytes[i] != 0x00)
                    return 0;
            return 1;
        }

        int is_base() const {
            if (bytes[0] != 0x58)
                return 0;
            for (int i = 1; i < crypto_core_ed25519_BYTES; ++i)
                if (bytes[i] != 0x66)
                    return 0;
            return 1;
        }

        G_elt operator*(G_elt const& b) const {
            G_elt c;
            int ret;
            ret = crypto_core_ed25519_add(c.bytes, bytes, b.bytes);
            assert (ret == 0);
            return c;
        }

        G_elt pow(unsigned int m) const {
            Zq_elt k(m);
            return pow(k);
        }

        G_elt pow(Zq_elt const& k) const {
            G_elt c;
            int ret;
            if (is_base())
                ret = crypto_scalarmult_ed25519_base_noclamp(c.bytes, k.k);
            else
                ret = crypto_scalarmult_ed25519_noclamp(c.bytes, k.k, bytes);
            assert (ret == 0);
            COUNT_POW++;
            return c;
        }

        G_elt operator/(G_elt const& b) const {
            G_elt c;
            int ret;
            ret = crypto_core_ed25519_sub(c.bytes, bytes, b.bytes);
            assert (ret == 0);
            return c;
        }

        bool operator==(G_elt const& b) const {
            for (int i = 0; i < crypto_core_ed25519_BYTES; ++i) {
                if (b.bytes[i] != bytes[i])
                    return false;
            }
            return true;
        }

        G_elt invert() const {
            G_elt z; // initialized to neutral
            crypto_core_ed25519_sub(z.bytes, z.bytes, bytes);
            return z;
        }

        G_elt sqrt() const {
            Zq_elt k = Zq_elt::qp1s2();
            return this->pow(k);
        }

        int dlog() const {
            G_elt G = G_elt::base();
            if (is_neutral())
                return 0;
            G_elt Gm = G;
            G_elt Gmm = Gm.invert();
            int m = 1;
            while (1) {
                if (Gm == *this)
                    return m;
                if (Gmm == *this)
                    return -m;
                m++;
                Gm = Gm*G;
                Gmm = Gmm/G;
            }
        }

        std::string serialize() const {
            std::string s;
            for (int i = 0; i < crypto_core_ed25519_BYTES; ++i) {
                unsigned char c = bytes[i];
                char c0, c1;
                to_hex(c0, c1, c);
                s = (s + c1) + c0;
            }
            return s;
        }

        friend std::ostream& operator<<(std::ostream& os, const G_elt& k);
};

inline std::ostream& operator<<(std::ostream& os, const G_elt& k) {
    for (int i = 0; i < crypto_core_ed25519_BYTES; ++i) {
        unsigned char c = k.bytes[i];
        char c0, c1;
        to_hex(c0, c1, c);
        os << c1 << c0;
    }
    return os;
}



#endif   /* __GROUP_H__ */
