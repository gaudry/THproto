#ifndef __CONDORCET_H__
#define __CONDORCET_H__

#include "comp_model.h"

/////////// Preference Matrix
//
// B is a ballot of this form:
//   - there are k candidates
//   - each one receives a grade, as an integer over l bits
//     (typically, 2^l >= k, in order to set a proper order, but equality
//     are allowed)
// Compute the corresponding preference matrix M, where M[i,j] = 1 if i
// is preferred over j and 0 otherwise.
// (note that "preferred" means a lower grade)
std::vector<Node *> PrefMatrix(net_info *NI, std::string tag,
        std::vector<std::vector<DataKey>> B,
        std::vector<std::vector<DataKey>> M,
        std::vector<NodeTag> dep);

/////////// Adjacency Matrix
//
// Takes as input an agglomerated Pref matrix M
// Computes the corresponding Adj Matrix A (same size)
//
// Both matrices contain bit-encoded positive integers (hence the triple
// layer of vectors)
std::vector<Node *> AdjMatrix(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> M,
        std::vector<std::vector<std::vector<DataKey>>> A,
        std::vector<NodeTag> dep);

//////////// Floyd-Warshall
//
// Takes as input an adjacency matrix S, which is a k x k matrix of
// l-bit integers.
// Computes for each (i,j) the strengh of the strongest path from i to j
//
// In-place: this destroys S.
std::vector<Node *> FloydWarshall(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> S,
        std::vector<NodeTag> dep);


//////////// Main Condorcet-Schulze function
// 
// Input is a list of ballots (as described in input of PrefMatrix)
// Output is a list of bit, one for each candidate, indicating if she is
// a winner.
std::vector<Node *> CondorcetSchulze(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> B,
        std::vector<DataKey> Winner,
        std::vector<NodeTag> dep);

// Other functions, to do on-the-fly computations
std::vector<Node *> PreAgglomerate(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> B,
        std::vector<std::vector<std::vector<DataKey>>> Magg,
        std::vector<NodeTag> dep);

std::vector<Node *> PostAgglomerate(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<std::vector<DataKey>>>> Magg_list,
        std::vector<std::vector<std::vector<DataKey>>> Magg,
        std::vector<NodeTag> dep);

std::vector<Node *> FinishCondorcet(net_info *NI, std::string tag,
        std::vector<std::vector<std::vector<DataKey>>> Magg,
        std::vector<DataKey> Winner,
        std::vector<NodeTag> dep);

#endif   /* __CONDORCET_H__ */
