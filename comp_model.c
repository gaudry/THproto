#include <cassert>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>


#include "comp_model.h"
#include "networking.h"

unsigned long TRANSCRIPT_SIZE = 0;

std::unordered_map<NodeTag, Node> Node::all_nodes;
std::unordered_map<std::string, Data> *Node::received_data;

std::function<bool (const Node *, const Node *)> NodeRkCmp = [](const Node * a, const Node * b) {
    return (a->rank > b->rank);
};

// A message has the following format:
//   size:tag:nb_item:type:data:type:data:...
// where
//   size  is the number of bytes of the string after "size:"
//   tag   is a string to match send / recv
//   nb_item is the number of items for this tag
//   type  is an integer indicating the type of the data (as index of variant)
//   data  is the serialized data
//   the last two are repeated nb_item times

void Node::send_run() {
    NI->send_mutex[peer].lock();
    std::string s;
    ctx_mtx.lock();
    for(auto & inp : input) {
        s = s + ":" + std::to_string(Context.data[inp].index());
        s = s + ":" + GlobalContext::serialize(Context.data[inp]);
    }
    ctx_mtx.unlock();
    s = tag + ":" + std::to_string(input.size()) + s;
    std::string n = std::to_string(s.length());
    s = n + ":" + s;
    ssize_t xx = 0;
    ssize_t nn = s.length();
    const char *cc = s.c_str();
    do {
        ssize_t ret = send(NI->send_sock[peer], cc+xx, nn-xx, 0);
        assert (ret > 0);
        xx += ret;
        assert (xx <= nn);
    } while (xx < nn);
    NI->send_mutex[peer].unlock();
}

// Permanently listen to a peer and fill-in a data structure with what is
// received, to be used by recv_nodes.

bool has_complete_entry(ssize_t *n, std::string & str) {
    size_t pos = str.find(std::string(":"));
    if (pos == std::string::npos)
        return false;
    std::string s = str.substr(0, pos);
    *n = std::stol(s);
    return (str.size() >= (*n + pos + 1));
}
void Node::recv_loop(const net_info * NI, int peer, NodeDAG * dag) {
    std::string current_string;
    while (1) {
        // Read more stuff
        char buf[1024];
        ssize_t ret = read(NI->recv_sock[peer], buf, 1000);
        if (ret == 0) {
            std::cerr << "Peer " << peer
                << " has cut the communication. This is the end.\n";
            return;
        }
        buf[ret] = '\0';
        current_string = current_string + std::string(buf);

        // Check if we have a complete item
        ssize_t n;
        while (has_complete_entry(&n, current_string)) {
//            std::cerr << "Handling entry receveid from " << peer << ": " << current_string << "\n";
            // extract the substring with first entry
            ssize_t pos = current_string.find(std::string(":"));
            std::string str = current_string.substr(pos+1, pos+n+1);
            current_string = current_string.substr(pos+n+1, std::string::npos);
            // extract tag, nb_item, and the list of (index, data)
            pos = str.find(std::string(":"));
            std::string tag = str.substr(0, pos);
            ssize_t pos2 = str.find(std::string(":"), pos+1);
            std::string nb_it_s = str.substr(pos+1, pos2);
            int nb_it = std::stoi(nb_it_s);
            std::string u = str.substr(pos2+1);
            for(int i = 0; i < nb_it; ++i) {
                ssize_t pos_ind = u.find(std::string(":"));
                std::string ind = u.substr(0, pos_ind);
                int index = std::stoi(ind);
                u = u.substr(pos_ind+1);
                ssize_t pos_dat = u.find(std::string(":"));
                std::string data = u.substr(0, pos_dat);
                std::string tagi = tag + "item" + std::to_string(i);
                NI->recv_mutex[peer].lock();
                ctx_mtx.lock();
                Node::received_data[peer][tagi] = GlobalContext::unserialize(data, index);
                ctx_mtx.unlock();
                NI->recv_mutex[peer].unlock();
                if (i < nb_it-1) {
                    u = u.substr(pos_dat+1);
                }
            }
            dag->notify_update(NULL);
        }
    }
}

void Node::recv_run() {
    // std::cerr << "Entering recv_run with tag " << tag << "\n";
    bool got_all = true;
    NI->recv_mutex[peer].lock();
    ctx_mtx.lock();
    got_all = true;
    for(int i = 0; i < output.size(); ++i) {
        DataKey out = output[i];
        if (Context.data.find(out) != Context.data.end())
            continue;
        std::string tagi = tag + "item" + std::to_string(i);
        if (Node::received_data[peer].find(tagi) != Node::received_data[peer].end()) {
            Context.data[out] = Node::received_data[peer][tagi];
            // std::cerr << "Read data with tag " << tagi << "\n";
        } else {
            assert(0); // should never arrive here
            got_all = false;
        }
    }
    ctx_mtx.unlock();
    NI->recv_mutex[peer].unlock();
}




