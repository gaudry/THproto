#include <cassert>

#include "comp_model.h"
#include "elgamal.h"
#include "cgate.h"

int random_bit() {
    uint32_t r = randombytes_random();
    return (r & 1U);
}

void Dec_comp_1(std::vector<DataKey> out, std::vector<DataKey> inp) {
    Ciphertext X = std::get<Ciphertext>(Context.get(inp[0]));
    Zq_elt sk = std::get<Zq_elt>(Context.get("sk"));
    G_elt part = Ciphertext::partial_decrypt(X, sk);
    Zq_elt alpha = Zq_elt::random();
    G_elt e1 = G_elt::base().pow(alpha);
    G_elt e2 = X.c1.pow(alpha);
    G_elt my_pk = std::get<G_elt>(Context.get("mypk"));
    std::string to_hash =
        inp[1] + // hashtag
        GlobalContext::serialize(Context.get("pk")) +
        my_pk.serialize() +
        X.serialize() +
        part.serialize() +
        e1.serialize() + 
        e2.serialize();
    Zq_elt d = Zq_elt::hash(to_hash);
    Zq_elt ans = alpha + sk*d;
    Dec_ZKP zkp(e1, e2, ans);
    Context.set(out[0], part);
    Context.set(out[1], zkp);
}

void Dec_comp_2(std::vector<DataKey> out, std::vector<DataKey> inp) {
    int n = (inp.size()-2) / 2;
    std::vector<G_elt> part;
    std::vector<Dec_ZKP> zkp;
    for (int i = 0 ; i < n; ++i) {
        part.push_back(std::get<G_elt>(Context.get(inp[2*i])));
        zkp.push_back(std::get<Dec_ZKP>(Context.get(inp[2*i+1])));
    }
    Ciphertext X = std::get<Ciphertext>(Context.get(inp[2*n]));
    std::string hashtag = inp[2*n+1];
    int me = std::get<int>(Context.get("me"));
    G_elt pk = std::get<G_elt>(Context.get("pk"));
    // check ZKP
    for (int i = 0 ; i < n; ++i) {
        // skip my own ZKP
        if (i == me)
            continue;
        G_elt pki = std::get<G_elt>(Context.get("pk" + std::to_string(i)));
        std::string to_hash =
            hashtag +
            GlobalContext::serialize(pk) +
            GlobalContext::serialize(pki) +
            X.serialize() +
            part[i].serialize() +
            zkp[i].e1.serialize() + 
            zkp[i].e2.serialize();
        Zq_elt d = Zq_elt::hash(to_hash);
        G_elt tmp = G_elt::base().pow(zkp[i].ans)/pki.pow(d);
        assert (tmp == zkp[i].e1);
        tmp = X.c1.pow(zkp[i].ans)/part[i].pow(d);
        assert (tmp == zkp[i].e2);
    }
    // OK to combine, now.
    Context.set(out[0], Ciphertext::combine_partial_decrypt(X, part));
}

std::vector<Node *> Dec(net_info *NI, std::string tag, DataKey X,
        DataKey Z, std::vector<NodeTag> dep) {
    std::vector<Node *> result;
    int me = NI->me;
    std::string my_partial = tag + "partial" + std::to_string(me);
    std::string my_zkp = tag + "zkp" + std::to_string(me);
    std::string hashtag = tag + "hashtag";
    Context.data[hashtag] = std::string("");

    // Compute alpha^sk and ZKP
    std::vector<DataKey> out = { my_partial, my_zkp };
    std::vector<DataKey> in = { X, hashtag };
    Node * N = Node::CompNode(NI, tag + "comp1", dep, Dec_comp_1,
            in, out);
    result.push_back(N);
    std::vector<NodeTag> dep2;
    dep2.push_back(N->ntag);

    // Broadcast them
    std::vector<Node *> NB = Node::BroadcastNodes(NI, tag + "comm1", dep2,
            std::vector<DataKey> { my_partial, my_zkp });
    TRANSCRIPT_SIZE += G_elt::size + Dec_ZKP::size;
    for (auto N : NB) {
        result.push_back(N);
    }

    std::vector<NodeTag> dep3 = { dep2 };
    // Receive other contributions
    for (int i = 0; i < NI->pub_keys.size(); ++i) {
        if (i == me)
            continue;
        std::vector<DataKey> out = { tag + "partial" + std::to_string(i),
            tag + "zkp" + std::to_string(i) };
        Node *N = Node::RecvNode(NI, tag + "comm1", dep, out, i);
        result.push_back(N);
        dep3.push_back(N->ntag);
    }

    // Compute result
    std::vector<DataKey> inp;
    for (int i = 0; i < NI->pub_keys.size(); ++i) {
        inp.push_back(tag + "partial" + std::to_string(i));
        inp.push_back(tag + "zkp" + std::to_string(i));
    }
    inp.push_back(X);
    inp.push_back(hashtag);
    Node *Nf = Node::CompNode(NI, tag + "comp2", dep3, Dec_comp_2, inp,
            std::vector<DataKey> {Z});
    result.push_back(Nf);

    return result;
}

////////////////////////////////////////////////////////////////////

// Step 1 of CGate for trustee 0.
void CGate_comp_code_1_0(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 2);
    assert (out.size() == 7);
    Ciphertext X = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext Y = std::get<Ciphertext>(Context.get(inp[1]));
    G_elt pk = std::get<G_elt>(Context.get("pk"));
    Ciphertext Xm1 = X;
    Ciphertext Ym1 = Y*Y*Ciphertext::Eminus1();
    // some randomness
    int s = 2*random_bit()-1;
    Zq_elt r1 = Zq_elt::random();
    Zq_elt r2 = Zq_elt::random();

    // Compute X0, Y0
    Ciphertext X0, Y0;
    if (s == 1) {
        X0 = Xm1.ReEnc(pk, r1);
        Y0 = Ym1.ReEnc(pk, r2);
    } else {
        X0 = Xm1.invert().ReEnc(pk, r1);
        Y0 = Ym1.invert().ReEnc(pk, r2);
    }
    
    // Save data
    Context.set(out[0], Xm1);
    Context.set(out[1], Ym1);
    Context.set(out[2], X0);
    Context.set(out[3], Y0);
    Context.set(out[4], s);
    Context.set(out[5], r1);
    Context.set(out[6], r2);
}

// Step 1 of CGate for trustee <>0.
void CGate_comp_code_1_o(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 4);
    assert (out.size() == 7);
    G_elt pk = std::get<G_elt>(Context.get("pk"));
    Ciphertext X    = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext Y    = std::get<Ciphertext>(Context.get(inp[1]));
    Ciphertext Xim1 = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext Yim1 = std::get<Ciphertext>(Context.get(inp[3]));
    Ciphertext Xm1 = X;
    Ciphertext Ym1 = Y*Y*Ciphertext::Eminus1();
    // some randomness
    int s = 2*random_bit()-1;
    Zq_elt r1 = Zq_elt::random();
    Zq_elt r2 = Zq_elt::random();

    // Compute Xi, Yi
    Ciphertext Xi, Yi;
    if (s == 1) {
        Xi = Xim1.ReEnc(pk, r1);
        Yi = Yim1.ReEnc(pk, r2);
    } else {
        Xi = Xim1.invert().ReEnc(pk, r1);
        Yi = Yim1.invert().ReEnc(pk, r2);
    }
    
    // Save data
    Context.set(out[0], Xm1);
    Context.set(out[1], Ym1);
    Context.set(out[2], Xi);
    Context.set(out[3], Yi);
    Context.set(out[4], s);
    Context.set(out[5], r1);
    Context.set(out[6], r2);
}

// Step 2 of CGate (ZKP)
//   Input : Xi-1, Yi-1, Xi, Yi, s, r1, r2
//   Output: pii
void CGate_comp_code_2(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 8);
    assert (out.size() == 1);
    G_elt pk = std::get<G_elt>(Context.get("pk"));
    Ciphertext Xim1 = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext Yim1 = std::get<Ciphertext>(Context.get(inp[1]));
    Ciphertext Xi = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext Yi = std::get<Ciphertext>(Context.get(inp[3]));
    int s = std::get<int>(Context.get(inp[4]));
    Zq_elt r1 = std::get<Zq_elt>(Context.get(inp[5]));
    Zq_elt r2 = std::get<Zq_elt>(Context.get(inp[6]));
    int ss = (s+1) >> 1; // convert -1 / 1  to  0 / 1
    int ms = 1-ss;
    Zq_elt alpha = Zq_elt::random();
    Zq_elt beta = Zq_elt::random();
    Ciphertext eX[2], eY[2];
    eX[ss] = Ciphertext(pk, 0, alpha);
    eY[ss] = Ciphertext(pk, 0, beta);
    Zq_elt sigma[2], rhoX[2], rhoY[2];
    sigma[ms] = Zq_elt::random();
    rhoX[ms] = Zq_elt::random();
    rhoY[ms] = Zq_elt::random();
    Ciphertext tmpX = Xim1;
    Ciphertext tmpY = Yim1;
    if (s == 1) {
        tmpX = tmpX*Xi;
        tmpY = tmpY*Yi;
    } else {
        tmpX = Xi / tmpX;
        tmpY = Yi / tmpY;
    }
    tmpX = tmpX.pow(-sigma[ms]);
    tmpY = tmpY.pow(-sigma[ms]);
    eX[ms] = Ciphertext(pk, 0, rhoX[ms])*tmpX;
    eY[ms] = Ciphertext(pk, 0, rhoY[ms])*tmpY;
    std::string to_hash =
        inp[7] +
        pk.serialize() +
        Xim1.serialize() +
        Yim1.serialize() +
        Xi.serialize() +
        Yi.serialize() +
        eX[0].serialize() + 
        eY[0].serialize() + 
        eX[1].serialize() + 
        eY[1].serialize();
    Zq_elt d = Zq_elt::hash(to_hash);
    sigma[ss] = d - sigma[ms];
    rhoX[ss] = alpha + r1*sigma[ss];
    rhoY[ss] = beta  + r2*sigma[ss];
    CG_ZKP_1 pi(eX[0], eY[0], eX[1], eY[1], sigma[0], sigma[1],
            rhoX[0], rhoY[0], rhoX[1], rhoY[1]);
    Context.set(out[0], pi);
}

void CGate_comp_code_3(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert (inp.size() == 6);
    assert (out.size() == 0);
    G_elt pk = std::get<G_elt>(Context.get("pk"));
    Ciphertext Xim1 = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext Yim1 = std::get<Ciphertext>(Context.get(inp[1]));
    Ciphertext Xi = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext Yi = std::get<Ciphertext>(Context.get(inp[3]));
    CG_ZKP_1 pi = std::get<CG_ZKP_1>(Context.get(inp[4]));
    std::string to_hash =
        inp[5] +
        pk.serialize() +
        Xim1.serialize() +
        Yim1.serialize() +
        Xi.serialize() +
        Yi.serialize() +
        pi.eX0.serialize() + 
        pi.eY0.serialize() + 
        pi.eX1.serialize() + 
        pi.eY1.serialize();
    Zq_elt d = Zq_elt::hash(to_hash);
    assert (pi.sigma0 + pi.sigma1 == d);
    assert (Ciphertext(pk, 0, pi.rhoX0)*(Xi*Xim1).pow(-pi.sigma0) == pi.eX0);
    assert (Ciphertext(pk, 0, pi.rhoY0)*(Yi*Yim1).pow(-pi.sigma0) == pi.eY0);
    assert (Ciphertext(pk, 0, pi.rhoX1)*(Xi/Xim1).pow(-pi.sigma1) == pi.eX1);
    assert (Ciphertext(pk, 0, pi.rhoY1)*(Yi/Yim1).pow(-pi.sigma1) == pi.eY1);
}

// Re-randomization part
void CGate_comp_code_4(std::vector<DataKey> out, std::vector<DataKey> inp)
{
    assert (inp.size() == 3);
    assert (out.size() == 4);
    Ciphertext Xa  = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext Ya  = std::get<Ciphertext>(Context.get(inp[1]));
    G_elt pk = std::get<G_elt>(Context.get("pk"));

    Zq_elt alphaX = Zq_elt::random();
    Zq_elt alphaY = Zq_elt::random();
    Ciphertext A = Ciphertext(pk, 0, alphaX);
    Ciphertext B = Ciphertext(pk, 0, alphaY);
    // Compute ZKPs
    Zq_elt wX = Zq_elt::random();
    Zq_elt wY = Zq_elt::random();
    Ciphertext eX = Ciphertext(pk, 0, wX);
    Ciphertext eY = Ciphertext(pk, 0, wY);
    Zq_elt dX = Zq_elt::hash(pk.serialize() + A.serialize() + eX.serialize());
    Zq_elt dY = Zq_elt::hash(pk.serialize() + B.serialize() + eY.serialize());
    CG_ZKP_2 pi(eX, wX+alphaX*dX, eY, wY+alphaY*dY);
    // Compute commitment
    Zq_elt c = Zq_elt::hash(inp[2] + pk.serialize() +
            Xa.serialize() + Ya.serialize() +
            A.serialize() + B.serialize());
    Context.set(out[0], A);
    Context.set(out[1], B);
    Context.set(out[2], c);
    Context.set(out[3], pi);
}

void CGate_comp_code_5(std::vector<DataKey> out, std::vector<DataKey> inp)
{
    assert (out.size() == 2);
    int n = (inp.size()-5) / 4;
    std::string hashtag = inp[inp.size()-1];

    G_elt pk = std::get<G_elt>(Context.get("pk"));
    Ciphertext Xprime  = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext Yprime  = std::get<Ciphertext>(Context.get(inp[1]));
    Ciphertext Xa  = std::get<Ciphertext>(Context.get(inp[2]));
    Ciphertext Ya  = std::get<Ciphertext>(Context.get(inp[3]));
    for (int i = 0; i < n; ++i) {
        // Check ZKP
        Ciphertext Ai = std::get<Ciphertext>(Context.get(inp[4*i+4]));
        Ciphertext Bi = std::get<Ciphertext>(Context.get(inp[4*i+5]));
        Zq_elt ci     = std::get<Zq_elt>(Context.get(inp[4*i+6]));
        CG_ZKP_2 pi   = std::get<CG_ZKP_2>(Context.get(inp[4*i+7]));
        assert(ci == Zq_elt::hash(hashtag + pk.serialize() +
            Xa.serialize() + Ya.serialize() +
            Ai.serialize() + Bi.serialize()));
        Zq_elt dX = Zq_elt::hash(pk.serialize() +
                Ai.serialize() + pi.eX.serialize());
        Zq_elt dY = Zq_elt::hash(pk.serialize() +
                Bi.serialize() + pi.eY.serialize());
        assert(Ciphertext(pk, 0, pi.aX)/Ai.pow(dX) == pi.eX);
        assert(Ciphertext(pk, 0, pi.aY)/Bi.pow(dY) == pi.eY);
        // Accumulate Xprime,Yprime
        Xprime = Xprime*Ai;
        Yprime = Yprime*Bi;
    }
    Context.set(out[0], Xprime);
    Context.set(out[1], Yprime);
}

void CGate_comp_code_final(std::vector<DataKey> out, std::vector<DataKey> inp)
{
    assert (inp.size() == 3);
    assert (out.size() == 1);
    Ciphertext X  = std::get<Ciphertext>(Context.get(inp[0]));
    Ciphertext Xp = std::get<Ciphertext>(Context.get(inp[1]));
    G_elt decY = std::get<G_elt>(Context.get(inp[2]));

    int decy = decY.dlog();
    Ciphertext tmp = X*Xp.pow(decy);
    Context.set(out[0], tmp.sqrt());
}

////////////////////////////
//  Main CGage function
////////////////////////////

std::vector<Node *> CGate(net_info *NI, std::string tag, DataKey X, DataKey Y,
        DataKey Z, std::vector<NodeTag> dep) {
    std::string hashtag = tag + "hashtag";
    int me = NI->me;
    std::vector<Node *> result;
    std::string tagXi, tagYi, tagXim1, tagYim1, tagXm1, tagYm1,
        tags, tagr1, tagr2, tagzkp1i, tagXa, tagYa, tagXprime, tagYprime;
    tagXi = tag + "X" + std::to_string(me);
    tagYi = tag + "Y" + std::to_string(me);
    tagXim1 = tag + "X" + std::to_string(me-1);
    tagYim1 = tag + "Y" + std::to_string(me-1);
    tagXm1 = tag + "X" + std::to_string(-1);
    tagYm1 = tag + "Y" + std::to_string(-1);
    tags = tag + "s";
    tagr1 = tag + "r1";
    tagr2 = tag + "r2";
    tagzkp1i = tag + "zkp1" + std::to_string(me);
    tagXa = tag + "X" + std::to_string(NI->pub_keys.size()-1);
    tagYa = tag + "Y" + std::to_string(NI->pub_keys.size()-1);
    tagXprime = tag + "Xprime";
    tagYprime = tag + "Yprime";
    std::vector<NodeTag> dep0;
    // All trustees wait for the previous one, except trustee 0
    if (me != 0) {
        std::vector<DataKey> out = { tagXim1, tagYim1 };
        Node *N = Node::RecvNode(NI, tag + "comm1", dep, out, me-1);
        result.push_back(N);
        dep0.push_back(N->ntag);
    }
    // Compute X_i, Y_i (and X_{-1}, Y_{-1})
    if (me == 0) {
        std::vector<DataKey> in = { X, Y };
        std::vector<DataKey> out = { tagXim1, tagYim1, tagXi, tagYi,
            tags, tagr1, tagr2 };
        Node *N = Node::CompNode(NI, tag + "step1", dep, CGate_comp_code_1_0,
                in, out);
        result.push_back(N);
    } else {
        std::vector<DataKey> in = { X, Y, tagXim1, tagYim1 };
        std::vector<DataKey> out = { tagXm1, tagYm1, tagXi, tagYi,
            tags, tagr1, tagr2 };
        Node * N = Node::CompNode(NI, tag + "step1", dep0, CGate_comp_code_1_o,
                in, out);
        result.push_back(N);
    }
    std::vector<NodeTag> dep2 = { result.back()->ntag };
    // Send X_i, Y_i to everyone
    {
        std::vector<DataKey> in = { tagXi, tagYi };
        std::vector<Node *> NB = Node::BroadcastNodes(NI, tag + "comm1",
                dep2, in);
        TRANSCRIPT_SIZE += 2*Ciphertext::size;
        for (Node * N : NB) {
            result.push_back(N);
        }
    }
    // Compute ZKP_i
    {
        std::vector<DataKey> in = { tagXim1, tagYim1, tagXi, tagYi,
            tags, tagr1, tagr2, hashtag };
        std::vector<DataKey> out = {tagzkp1i};
        Node *N = Node::CompNode(NI, tag + "step2", dep2, CGate_comp_code_2,
                in, out);
        result.push_back(N);
    }
    std::vector<NodeTag> dep3 = { result.back()->ntag };
    // Send ZKP_i to everyone
    {
        std::vector<DataKey> in = { tagzkp1i };
        std::vector<Node *> NB = Node::BroadcastNodes(NI, tag + "comm2",
                dep3, in);
        TRANSCRIPT_SIZE += CG_ZKP_1::size;
        for (Node * N : NB) {
            result.push_back(N);
        }
    }

    std::vector<NodeTag> dep_zkp1;    // what depends on having checked ZKPs
    std::vector<NodeTag> dep_XYfinal; // what depends on receiving last Xi,Yi
    dep_XYfinal.push_back(dep2[0]);

    // Receive Xi, Yi and ZKP of others and check them
    {
        std::vector<NodeTag> zkpdep(dep0); // depends on first recv
        // First the missing Xi, Yi
        for (int i = 0; i < NI->pub_keys.size(); ++i) {
            if (i == me)
                continue;
            if (i != me-1) {
                // we also need to receive X_i, Y_i in those cases
                std::vector<DataKey> out = {
                    tag + "X" + std::to_string(i),
                    tag + "Y" + std::to_string(i) };
                Node *N = Node::RecvNode(NI, tag + "comm1", dep, out, i);
                result.push_back(N);
                zkpdep.push_back(N->ntag);
                if (i == NI->pub_keys.size()-1) {
                    dep_XYfinal.push_back(N->ntag);
                }
            }
        }
        // Now the Zkp
        for (int i = 0; i < NI->pub_keys.size(); ++i) {
            if (i == me)
                continue;
            std::vector<DataKey> out = { tag + "zkp1" + std::to_string(i) };
            Node *N = Node::RecvNode(NI, tag + "comm2", zkpdep, out, i);
            result.push_back(N);

            std::vector<NodeTag> localdep(zkpdep);
            localdep.push_back(N->ntag);
            localdep.push_back(dep2[0]);
            std::vector<DataKey> in = { 
                tag + "X" + std::to_string(i-1),
                tag + "Y" + std::to_string(i-1),
                tag + "X" + std::to_string(i),
                tag + "Y" + std::to_string(i),
                tag + "zkp1" + std::to_string(i),
                hashtag };
            out.clear();
            Node *NN = Node::CompNode(NI, tag + "comp3" + std::to_string(i),
                    localdep,
                    CGate_comp_code_3, in, out);
            result.push_back(NN);
            dep_zkp1.push_back(NN->ntag);
        }
    }

    // Rerandomization
    { 
        std::string tagAi = tag + "A" + std::to_string(me);
        std::string tagBi = tag + "B" + std::to_string(me);
        std::string tagci = tag + "c" + std::to_string(me);
        std::string tagzkp2i = tag + "zkp2" + std::to_string(me);
        // Compute share
        std::vector<DataKey> in = { tagXa, tagYa, hashtag };
        std::vector<DataKey> out = {tagAi, tagBi, tagci, tagzkp2i};
        Node *N = Node::CompNode(NI, tag + "comp4", dep_XYfinal,
                CGate_comp_code_4, in, out);
        result.push_back(N);
        std::vector<NodeTag> dep2 = { result.back()->ntag };
        // Send the commitment
        std::vector<DataKey> in2 = {tagci};
        std::vector<Node *> NB = Node::BroadcastNodes(NI, tag + "comm3",
                dep2, in2);
        TRANSCRIPT_SIZE += Zq_elt::size;
        for (Node * N : NB) {
            result.push_back(N);
        }
        std::vector<NodeTag> dep3;
        // Receive other commitments
        for (int i = 0; i < NI->pub_keys.size(); ++i) {
            if (i == me)
                continue;
            std::vector<DataKey> out = { tag + "c" + std::to_string(i) };
            Node * N = Node::RecvNode(NI, tag + "comm3", dep2, out, i);
            result.push_back(N);
            dep3.push_back(N->ntag);
        }
        // Reveal randomization data
        std::vector<DataKey> in3 = { tagAi, tagBi, tagzkp2i };
        NB = Node::BroadcastNodes(NI, tag + "comm4", dep3, in3);
        TRANSCRIPT_SIZE += 2*Ciphertext::size + CG_ZKP_2::size;
        for (Node * N : NB) {
            result.push_back(N);
        }
        std::vector<NodeTag> dep4;
        // Receive other randomization data
        for (int i = 0; i < NI->pub_keys.size(); ++i) {
            if (i == me)
                continue;
            std::vector<DataKey> out = {
                tag + "A" + std::to_string(i),
                tag + "B" + std::to_string(i),
                tag + "zkp2" + std::to_string(i) };
            Node * N = Node::RecvNode(NI, tag + "comm4", dep3, out, i);
            result.push_back(N);
            dep4.push_back(N->ntag);
        }
        // Check and compute the new X', Y'
        in = { tagXa, tagYa, tagXa, tagYa };
        for (int i = 0; i < NI->pub_keys.size(); ++i) {
            in.push_back(tag + "A" + std::to_string(i));
            in.push_back(tag + "B" + std::to_string(i));
            in.push_back(tag + "c" + std::to_string(i));
            in.push_back(tag + "zkp2" + std::to_string(i));
        }
        in.push_back(hashtag);
        out = {tagXprime, tagYprime};
        N = Node::CompNode(NI, tag + "comp5", dep4, 
                CGate_comp_code_5, in, out);
        result.push_back(N);
    }

    std::vector<NodeTag> dep5 = { result.back()->ntag };
    // Decrypt the last (re-randomized) Y
    {
        std::vector<Node *> NN = Dec(NI, tag + "dec",
                tagYprime,
                tag + "decFinalY", dep5);
        for (auto N : NN)
            result.push_back(N);
    }
    // Final computation
    {
        std::vector<DataKey> in = {
            X, tagXprime, tag + "decFinalY" };
        Node * N = Node::CompNode(NI, tag + "compfinal",
                std::vector<NodeTag> { result.back()->ntag },
                CGate_comp_code_final, in,
                std::vector<DataKey> { Z });
        result.push_back(N);
    }
    return result;
}

void sync_point(std::vector<DataKey> out, std::vector<DataKey> inp) {
    assert(out.size() == 0);
    assert(inp.size() == 0);
}

std::vector<Node *> CSZBits(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        DataKey Y,
        std::vector<DataKey> Z, std::vector<NodeTag> dep) {
    std::vector<Node *> result;
    int n = X.size();
    assert (Z.size() == n);
    std::vector<NodeTag> finaldep;
    for (int i = 0; i < n; ++i) {
        std::vector<Node *> NN = CGate(NI,
                tag + std::to_string(i),
                X[i], Y, Z[i],
                dep);
        for (Node * z : NN)
            result.push_back(z);
        finaldep.push_back(result.back()->ntag);
    }
    Node * N = Node::CompNode(NI, tag + "final",
            finaldep, sync_point,
            std::vector<DataKey> {  },
            std::vector<DataKey> {  });
    result.push_back(N);
    return result;
}

// TODO: naive implementation. Could use a tree-based approach, here.
std::vector<Node *> CSZAcc(net_info *NI, std::string tag,
        std::vector<DataKey> X,
        DataKey Y,
        std::vector<NodeTag> dep)
{
    std::vector<Node *> result;
    int n = X.size();
    DataKey R = tag + "R";

    std::vector<NodeTag> curr_dep = { dep };

    for (int i = 1; i < n; ++i) {
        DataKey _R;
        DataKey _Z;
        if (i == 1) {
            _R = X[0];
        } else {
            _R = R;
        }
        if (i == n-1) {
            _Z = Y;
        } else {
            _Z = R;
        }
        std::vector<Node *> NN = CGate(NI, tag + std::to_string(i),
                X[i], _R, _Z,
                curr_dep);
        for (Node * z : NN)
            result.push_back(z);
        curr_dep = { result.back()->ntag };
    }
    return result;
}
