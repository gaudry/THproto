#include <iostream>
#include <cassert>
#include <cstring>
#include <cstdlib>
#include "group.h"
#include "elgamal.h"

unsigned long COUNT_POW = 0;

bool CheckZKP(Ciphertext c, ZKP01 pi, G_elt pk)
{
    Zq_elt d = Zq_elt::hash( pk.serialize() + c.serialize() +
            pi.e0.serialize() + pi.e1.serialize());
    assert (pi.sigma0 + pi.sigma1 == d);
    assert (pi.e0
            ==
            Ciphertext(pk, 0, pi.rho0)*(c/Ciphertext::E0()).pow(-pi.sigma0));
    assert (pi.e1
            ==
            Ciphertext(pk, 0, pi.rho1)*(c/Ciphertext::E1()).pow(-pi.sigma1));
    return true;
}
   

// usage: ./checkballots pk k < ballots
// where pk is the public key of the election and k is the number of
// candidates.
// Ballots are read on stdin, in the same format as the one created by
// the 'ballot' binary.
int main(int argc, char **argv) {
    assert (argc == 3);
    // Election parameters
    G_elt pk = G_elt(argv[1]);
    int k = atoi(argv[2]);
    int m = 1;
    while ( (k-1) > (1<<m)-1 )
        m++;

    int c = 0;
    while (1) {
        std::string line;

        bool end = false;
        std::vector<std::vector<Ciphertext>> C;
        std::vector<std::vector<ZKP01>> Pi;
        for (int i = 0; i < k; ++i) {
            C.push_back(std::vector<Ciphertext> { });
            Pi.push_back(std::vector<ZKP01> { });
            for (int j = 0; j < m; ++j) {
                std::string line;
                if (!std::getline(std::cin, line)) {
                    end = true;
                    break;
                }
                Ciphertext c(line.c_str());
                std::getline(std::cin, line);
                ZKP01 pi(line.c_str());
                CheckZKP(c, pi, pk);
            }
            if (end)
                break;
        }
        if (end)
            break;
        c++;
    }
    std::cout << "Checked " << c << " ballots\n";
    std::cout << "Performed " << COUNT_POW << " scal. mult.\n";
    return EXIT_SUCCESS;
}
