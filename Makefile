LIBS = -lpthread -lsodium
CC = g++
FLAGS = -g -O3 -std=c++17

.PHONY: all clean

all: ballot checkballots cond_trustee pre_agg post_agg cond_finish

SOURCES = arith.c add.c cgate.c networking.c comp_model.c condorcet.c

TRUSTEE_OBJ = $(patsubst %.c, %.o, $(SOURCES))
BALLOT_OBJ = 
CHECKBALLOTS_OBJ = 

COM_OBJECTS = $(patsubst %.c, %.o, $(COM_SOURCES))
HEADERS = $(wildcard *.h)

%.o: %.c $(HEADERS)
	$(CC) $(FLAGS) -c $< -o $@

cond_trustee: cond_trustee.o $(TRUSTEE_OBJ)
	$(CC) $(TRUSTEE_OBJ) cond_trustee.o  -Wall $(LIBS) -o cond_trustee

pre_agg: pre_agg.o $(TRUSTEE_OBJ)
	$(CC) $(TRUSTEE_OBJ) pre_agg.o  -Wall $(LIBS) -o pre_agg

post_agg: post_agg.o $(TRUSTEE_OBJ)
	$(CC) $(TRUSTEE_OBJ) post_agg.o  -Wall $(LIBS) -o post_agg

cond_finish: cond_finish.o $(TRUSTEE_OBJ)
	$(CC) $(TRUSTEE_OBJ) cond_finish.o  -Wall $(LIBS) -o cond_finish

ballot: ballot.o $(BALLOT_OBJ)
	$(CC) $(BALLOT_OBJ) ballot.o  -Wall $(LIBS) -o ballot

checkballots: checkballots.o $(CHECKBALLOTS_OBJ)
	$(CC) $(CHECKBALLOTS_OBJ) checkballots.o  -Wall $(LIBS) -o checkballots

clean:
	-rm -f *.o
	-rm -f $(TARGETS)
