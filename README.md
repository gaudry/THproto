Implementation of a Tally-Hiding toolbox
========================================

This is a prototype implementation of the Tally-Hiding functionalities of
the article *A toolbox for verifiable tally-hiding e-voting systems*, by
*Véronique Cortier, Pierrick Gaudry and Quentin Yang*. It is released
under the GNU GPLv3 license.

Only the ElGamal version is implemented.

This is a research prototype, not ready for production. It is not to be
used for real elections.

Compiling
---------

Requirements:
- A recent C++ compiler, supporting the C++17 standard (we use
  the `std::variant` data structure, which is not available in previous
  standards).  We used gcc 9.3.0.
- The libsodium library. We used version 1.0.18. A more recent version
  should work as well.
- A Linux environment. Other POSIX-compatible OSes might work, but are
  not tested. We use POSIX threads and POSIX network functions.

Then typing `make` should compile the `cond_trustee`, `ballot` and
`checkballots` binaries.

Running the program
-------------------

(Note that in the following examples, the public and secret keys are
consistent, and correspond to what is given in the example `conf` file.)

**Creating ballots**

The 'ballot' binary encrypts a vote and creates a ballots with the
appropriate zero-knowledge proofs. For example:

```
./ballot a958273641a348bd8b1f963cba30e6d4bcdbf375aa7582b8a2e5edc888226e60 2 0 2 3
```

produces a ballot encrypted with the given public key in an election with
4 candidates. The voter assigns the grades 2 to the first candidate, 0 to
the second, etc.

**Checking ZKP of ballots**
If a file contains the concatenation of the outputs of such ballot
creation procedures, the zkp can be checked using `checkballots`. In the
following example, 4 is the number of candidates:

```
./checkballots a958273641a348bd8b1f963cba30e6d4bcdbf375aa7582b8a2e5edc888226e60 4 < ballots_file
```

**Computing the tally-hiding Condorcet-Schulze result**
The 'cond_trustee' program emulates one trustee. It reads a file called 'conf'
which describes the network configuration and the public keys of all the
trustees.

In the given example, there are 3 trustees. One must therefore run 3
instances of the program, passing them the trustee number and the private
key (libsodium format):

```
./cond_trustee 0 16ebf61c126c8f3c96a329ff24a6eed4796333e51f6546c1bbcf2708779ec800 4 < ballots_file
./cond_trustee 1 51f7de7d6e683a00bdb76311ba9f30f4af4dfee01fef9494f3f467b988ffad0c 4 < ballots_file
./cond_trustee 2 41570b0692f017bdd08ba64e0a699f4b78b237ac7272773527a01a6779757509 4 < ballots_file
```

(they must be run in different terminals)

Then, the trustees are going to perform an MPC Condorcet-Schulze
tally-hiding decryption.

Even if all the trustees are run on the same machine, they will use the
loopback network interface.

**Decomposing the Condorcet-Schulze in several steps, for on-the-fly
computations**

As soon as ballots arrive, the trustees can start converting them to
preference matrices and to aggregate them. A few program can emulate this
behaviour. We explain this on an example.

When a bunch of (say, 32) ballots have arrived, they can be pre-aggregated as
follows:
```
./pre_agg 0 16ebf61c126c8f3c96a329ff24a6eed4796333e51f6546c1bbcf2708779ec800 5 <ballots_file  > /tmp/out0.0
./pre_agg 1 51f7de7d6e683a00bdb76311ba9f30f4af4dfee01fef9494f3f467b988ffad0c 5 <ballots_file > /tmp/out1.0
./pre_agg 2 41570b0692f017bdd08ba64e0a699f4b78b237ac7272773527a01a6779757509 5 <ballots_file > /tmp/out2.0
```
where 5 is the number of candidates. The output file will be identical.

Then, when a new bunch of 32 ballots arrive, run the same kind of
commands, and produce `/tmp/out0.1`, `/tmp/out1.1`, `/tmp/out2.1`. And so
on.

Once, all the ballots have been processed like so, all the intermediate
pre-aggregated matrices must be aggregated into a single matrix. This is
the purpose of the `post_agg` program. First create a file containing the
concatenation of all the outputs of `pre_agg`:
```
cat /tmp/out0.* > /tmp/out_pre
```

Then send it to `post_agg`:
```
./post_agg 0 16ebf61c126c8f3c96a329ff24a6eed4796333e51f6546c1bbcf2708779ec800 5  6 < /tmp/out_pre  > /tmp/out_post.0
./post_agg 1 51f7de7d6e683a00bdb76311ba9f30f4af4dfee01fef9494f3f467b988ffad0c 5  6 < /tmp/out_pre  > /tmp/out_post.1
./post_agg 2 41570b0692f017bdd08ba64e0a699f4b78b237ac7272773527a01a6779757509 5  6 < /tmp/out_pre  > /tmp/out_post.2
```
where the number 6 is the bit-width of the entries of `out_pre`. This
value is printed by `pre_agg`.

Again, the output files will be identical.

Finally, the aggregated matrix is sent to the `cond_finish` program:
```
./cond_finish 0 16ebf61c126c8f3c96a329ff24a6eed4796333e51f6546c1bbcf2708779ec800 5 9 < /tmp/out_post 
./cond_finish 1 51f7de7d6e683a00bdb76311ba9f30f4af4dfee01fef9494f3f467b988ffad0c 5 9 < /tmp/out_post
./cond_finish 2 41570b0692f017bdd08ba64e0a699f4b78b237ac7272773527a01a6779757509 5 9 < /tmp/out_post
```
where the number 9 is the bit-width of the entries, printed by
`post_agg`.

