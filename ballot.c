#include <iostream>
#include <cassert>
#include <cstring>
#include <cstdlib>
#include "group.h"
#include "elgamal.h"

unsigned long COUNT_POW = 0;

void EncryptBitAndProve(Ciphertext & c, ZKP01 & pi, int b, const G_elt & pk)
{
    assert ((b == 0) || (b == 1));
    Zq_elt r = Zq_elt::random();
    c = Ciphertext(pk, b, r);
    // ZKProof that c encrypts 0 or 1
    Zq_elt w = Zq_elt::random();
    Ciphertext e[2];
    Zq_elt sigma[2], rho[2];
    e[b] = Ciphertext(pk, 0, w);
    sigma[1-b] = Zq_elt::random();
    rho[1-b] = Zq_elt::random();
    Ciphertext E1mb;
    if (b == 1)
        E1mb = Ciphertext::E0();
    else
        E1mb = Ciphertext::E1();
    e[1-b] = Ciphertext(pk, 0, rho[1-b])*(c/E1mb).pow(-sigma[1-b]);
    Zq_elt d = Zq_elt::hash( pk.serialize() + c.serialize() +
            e[0].serialize() + e[1].serialize());
    sigma[b] = d - sigma[1-b];
    rho[b] = w + r*sigma[b];
    pi = ZKP01(e[0], e[1], sigma[0], rho[0], sigma[1], rho[1]);
}
    

// usage: ./ballot pk v_0 v_1 v_2 ... v_{k-1}
// where pk is the public key of the election and 
// where k is the number of candidates and v_i is the grade given to
// candidate i. v_i must be in [0, k-1], and the ballot will be encoded
// on the min number of bits that can represent k-1.
int main(int argc, char **argv) {
    // Public key
    G_elt pk = G_elt(argv[1]);

    // Store vote in a vector of int
    int k = argc-2;
    std::vector<int> v;
    for (int i = 2; i <= k+1; ++i) {
        int vi = atoi(argv[i]);
        assert ((vi >= 0) && (vi < k));
        v.push_back(vi);
    }

    // Number of bits to encode votes
    int m = 1;
    while ( (k-1) > (1<<m)-1 )
        m++;

    // Ballot creation
    std::vector<std::vector<Ciphertext>> C;
    std::vector<std::vector<ZKP01>> Pi;
    for (int i = 0; i < k; ++i) {
        C.push_back(std::vector<Ciphertext> { });
        Pi.push_back(std::vector<ZKP01> { });
        int V = v[i];
        for (int j = 0; j < m; ++j) {
            int bit = V & 1;
            Ciphertext c;
            ZKP01 pi;
            EncryptBitAndProve(c, pi, bit, pk);
            C[i].push_back(c);
            Pi[i].push_back(pi);
            V >>= 1;
        }
    }

    // Print to stdout. (Very) raw format...
    for (int i = 0; i < k; ++i) {
        for (int j = 0; j < m; ++j) {
            std::cout << C[i][j].serialize() << "\n";
            std::cout << Pi[i][j].serialize() << "\n";
        }
    }
    return EXIT_SUCCESS;
}
